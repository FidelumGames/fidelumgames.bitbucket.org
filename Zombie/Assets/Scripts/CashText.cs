﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CashText : MonoBehaviour {

    public Text text;
    public Stats stats;

    void Start () {
        text = GetComponent<Text>();
        stats = stats ? stats : GameObject.FindObjectOfType<Stats>();
	}
	
	public void UpdateUI() {
        Start();
        if (stats) {
            text.text = "$" + (int)stats.cash;
        }
    }
}
