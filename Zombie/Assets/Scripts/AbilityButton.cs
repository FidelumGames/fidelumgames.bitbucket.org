﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO: Ensure that values for each upgrade make sense, also ensure that formatting makes sense.

public class AbilityButton : MonoBehaviour {

    public int maxLevel = 5;
    public int currLevel = 0;
    public float[] levelValues;

    private Stats stats;
    private UIManager uiManager;
    private Shop shop;

    public int descriptionIndex;
    private string myDescription;
    public string myAbilityDescriptor;

    private List<string> descriptions = new List<string>();

    void Start() {
        uiManager = GameObject.FindObjectOfType<UIManager>();
        shop = GameObject.FindObjectOfType<Shop>();
        stats = GameObject.FindObjectOfType<Stats>();
        descriptions.Add("Allows you to jump in mid-air.");
        descriptions.Add("Improves your ability to repair your motorbike.");
        descriptions.Add("Increases your movement speed.");
        descriptions.Add("Increases the amount of experience gained from enemies.");
        descriptions.Add("Reduces the amount of damage dealt by enemies.");
        descriptions.Add("Increases the amount of money dropped by enemies.");
        descriptions.Add("Increases your maximum health.");
        descriptions.Add("Increases the range at which you will attract coins.");
        descriptions.Add("The rate at which you regenerate health.");
        descriptions.Add("Reduces the reload time for all weapons.");
        descriptions.Add("Increases your accuracy with all weapons.");
        myDescription = descriptions[descriptionIndex];
    }

    public void UpgradeMe() {
        shop.UpgradeAbility(this);
        ShowMyInfo();
    } 

    public void ShowMyInfo() {
        string name = gameObject.name;
        string cost = currLevel > 0 ? (currLevel + 1) + " skill points" : (currLevel + 1) + " skill point"; 


        string myInfo = name + 
            "\n\n" + myDescription +
            "\n\nCost: " + cost +
            "\n\nCurrent " + myAbilityDescriptor + ": " + GetCurrentStatValueAsString() + 
            "\n\nNext " + myAbilityDescriptor + ": " + GetNextStatValueAsString();

        if(currLevel == maxLevel) {
            myInfo = name +
            "\n\n" + myDescription +
            "\n\nCurrent " + myAbilityDescriptor + ": " + GetCurrentStatValueAsString() +
            "\n\nNext " + myAbilityDescriptor + ": " + GetNextStatValueAsString();
        }

        uiManager.ShowAbilityInfoPanel(myInfo);

    }

    public string GetCurrentStatValueAsString() {

        switch (gameObject.name) {       
            case "Multi-Jump":
                return stats.maxJumps + "";
            case "Grease Monkey":
                return stats.repairModifier + "";
            case "Fast Feet":
                return stats.runSpeed + "";              
            case "Fast Learner":
                return stats.XPMultiplier + "X";
            case "Iron Skin":
                return "%" + Mathf.CeilToInt(stats.damageResistance  * 100);
            case "Lucky":
                return stats.moneyMultiplier + "X";
            case "Juggernaut":
                return stats.healthMax + " HP";
            case "Attractive":
                return stats.magnetStrength + "";
            case "Hydra Man":
                return stats.regenAmount + " HP/s";
            case "Fast Fingers":
                return "%" + stats.reloadModifier * 100 + " reload speed";
            case "Steady Hands":
                return stats.accuracyModifier * 100 + " %  Accuracy  Bonus";
        }

        return "Error";
    }

    public string GetNextStatValueAsString() {

        if (currLevel < maxLevel) {

            switch (gameObject.name) {
                case "Multi-Jump":
                    return levelValues[currLevel] + "";
                case "Grease Monkey":
                    return (stats.repairModifier + 1) + "";
                case "Fast Feet":
                    return levelValues[currLevel] + "";
                case "Fast Learner":
                    return levelValues[currLevel] + "X";
                case "Iron Skin":
                    return "%" + Mathf.CeilToInt((stats.damageResistance - levelValues[currLevel]) * 100);
                case "Lucky":
                    return stats.moneyMultiplier + levelValues[currLevel] + "X";
                case "Juggernaut":
                    return (stats.healthMax + levelValues[currLevel]) + " HP";
                case "Attractive":
                    return levelValues[currLevel] + "";
                case "Hydra Man":
                    return levelValues[currLevel] + " HP/s";
                case "Fast Fingers":
                    return "%" + levelValues[currLevel] * 100 + " reload speed";
                case "Steady Hands":
                    return (stats.accuracyModifier + levelValues[currLevel]) * 100 + " %  Accuracy  Bonus";
            }
        } else {
            return "Max";
        }

        return "Error";
    }    
}
