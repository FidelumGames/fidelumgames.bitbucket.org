﻿using UnityEngine;
using System.Collections;

public class SeveredLimb : MonoBehaviour {

    private PlayerController player;
    private ParticleSystem particleSys;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D myRigidbody;
    private GameManager gameManager;

    public float maxForce, minForce, minTorque, maxTorque, fadeRate;
    private bool fade = false;
    private Color color;

	void Start () {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        player = GameObject.FindObjectOfType<PlayerController>();
        particleSys = GetComponentInChildren<ParticleSystem>();
        myRigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        color = spriteRenderer.color;

        float torque = Random.Range(maxTorque * -1, maxTorque);
        Vector2 force = new Vector2(Random.Range(minForce, maxForce), Random.Range(minForce, maxForce));

        if(player && player.transform.position.x > transform.position.x) {
            force.x *= -1;
        }

        myRigidbody.AddTorque(torque);
        myRigidbody.AddForce(force);

        foreach (ParticleSystem pSys in GetComponentsInChildren<ParticleSystem>()) {
            pSys.enableEmission = gameManager.goreEnabled;
        }

    }
	void Update () {
        if (particleSys && particleSys.time >= particleSys.duration) {
            fade = true;
        }        

        if (fade) {
            color.a -= fadeRate * Time.deltaTime;
            spriteRenderer.color = color;
            if(spriteRenderer.color.a <= 0) {
                Destroy(gameObject);
            }
        }
	}

    void OnCollisionEnter2D(Collision2D col) {
        if (!particleSys) {
            fade = true;
        }
    }
}
