﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FreeMoney : MonoBehaviour {

    private bool sponsorClicked, developerClicked = false;
    private AchievementManager achievementManager;
    private GameObject sponsorButton, developerButton;

    public GameObject linkPanel;

	void Start () {
        linkPanel = GameObject.Find("LinkPanel");
        sponsorButton = GameObject.Find("SponsorButton");
        developerButton = GameObject.Find("DeveloperButton");
        achievementManager = GameObject.FindObjectOfType<AchievementManager>();
        if (linkPanel) {
            linkPanel.SetActive(false);
        }
	}

    public void ClickSponsor() {
        sponsorClicked = true;
        achievementManager.UnlockSupporter();
        DoClick(sponsorButton);
    }

    public void ClickDeveloper() {
        developerClicked = true;       
        achievementManager.UnlockFriend();
        DoClick(developerButton);
    }    

    private void DoClick(GameObject button) {
        button.GetComponent<Button>().interactable = false;
        button.GetComponentInChildren<Text>().text = "THANKS!";
        if (sponsorClicked && developerClicked) {
            GameObject.FindObjectOfType<Stats>().SetIsOverUI(false);
            Destroy(linkPanel);
            Destroy(gameObject);
        }
    }

    public void TogglePanel() {
        linkPanel.SetActive(!linkPanel.activeSelf);
    }

    void Update() {
        if (GameObject.FindObjectOfType<Stats>() && Input.GetKeyDown(KeyCode.Escape)) {
            TogglePanel();
        } else if (!GameObject.FindObjectOfType<Stats>()) {
            Destroy(linkPanel);
            Destroy(gameObject);
        }
    }

}
