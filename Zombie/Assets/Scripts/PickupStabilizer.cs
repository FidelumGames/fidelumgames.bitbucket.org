﻿using UnityEngine;
using System.Collections;

public class PickupStabilizer : MonoBehaviour {


    public float yPos;
    public float fallSpeed = 4f;
    private Vector2 moveTemp;

	
    void Start() {
        moveTemp = transform.position;
    }

    void Update() {
        if (transform.position.y > yPos) {
            moveTemp.y -= fallSpeed * Time.deltaTime;
            if (moveTemp.y < yPos) {
                moveTemp.y = yPos;
            }
            transform.position = moveTemp;
        } else {
            Destroy(this);
        }
    }

}
