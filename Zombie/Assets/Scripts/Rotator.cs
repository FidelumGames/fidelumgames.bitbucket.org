﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Rotator : MonoBehaviour {

    public float rotationSpeed;
    private Vector3 rotation;
    private RectTransform rectTransform;

    void Start () {
        rectTransform = GetComponent<RectTransform>();
        rotation = rectTransform.eulerAngles;
	}
	
	
	void Update () {
        rotation.y += rotationSpeed * Time.deltaTime;
        rectTransform.eulerAngles = rotation;
	}
}
