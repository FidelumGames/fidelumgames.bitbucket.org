﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private AchievementManager achievementManager;
    public List<GameObject> introGameObjects = new List<GameObject>();
    public List<GameObject> mainGameObjects = new List<GameObject>();

    public bool goreEnabled = true;
    public bool casingsEnabled = true;

    public Texture2D cursor;

    void Start() {
        achievementManager = GameObject.FindObjectOfType<AchievementManager>();
        Cursor.SetCursor(cursor, new Vector2(16, 16), CursorMode.ForceSoftware);

        foreach (GameObject obj in mainGameObjects) {
            obj.SetActive(false);
        }

        if (mainGameObjects.Count > 0) {
           // Cursor.visible = false;
          
        }
    }

    public void Win() {
        achievementManager.UnlockFightAnotherDay();
        EndGame();
    }

    public void StartGame() {
        foreach (GameObject obj in mainGameObjects) {
            obj.SetActive(true);
        }

        Cursor.visible = true;
    }

    public void EndGame() {
        GameObject.FindObjectOfType<SpawnerManager>().enabled = false;
        foreach(Coin coin in GameObject.FindObjectsOfType<Coin>()) {
            Destroy(coin.gameObject);
        }
        foreach (AmmoPickup ammo in GameObject.FindObjectsOfType<AmmoPickup>()) {
            Destroy(ammo.gameObject);
        }
        foreach (HealthPickup health in GameObject.FindObjectsOfType<HealthPickup>()) {
            Destroy(health.gameObject);
        }
        GameObject.Find("NextWave").GetComponent<Text>().enabled = false;
        GameObject.Find("EnemiesRemaining").GetComponent<Text>().enabled = false;
        GameObject.Find("NextWaveButton").GetComponent<Button>().enabled = false;
        GameObject.Find("NextWaveButton").GetComponent<Image>().enabled = false;
        GameObject.Find("NextWaveButton/Text").GetComponent<Text>().enabled = false;
        GameObject.Find("AudioControls").SetActive(false);
        GameObject.Find("Gore").SetActive(false);
        GameObject.Find("Casings").SetActive(false);
        GameObject freeMoney = GameObject.Find("FreeMoney");
        if (freeMoney) {
            freeMoney.SetActive(false);
        }

        GameObject.Find("HealthSlider").SetActive(false);
        GameObject.Find("XPSlider").SetActive(false);
        GameObject.Find("ShopButton").SetActive(false);
        GameObject.Find("WeaponSlider").SetActive(false);
        GameObject.Find("AbilityPoints").SetActive(false);
        GameObject.FindObjectOfType<UIManager>().enabled = false;

        foreach (Collider2D col in GameObject.FindObjectsOfType<Collider2D>()) {
            col.enabled = false;
        }

        foreach (Enemy enemy in GameObject.FindObjectsOfType<Enemy>()) {
            enemy.EndDie();
        }

        foreach (Crow crow in GameObject.FindObjectsOfType<Crow>()) {
            crow.EndDie();
        }

        foreach (Spawner spawner in GameObject.FindObjectsOfType<Spawner>()) {
            spawner.enabled = false;
        }

        GameObject.FindObjectOfType<Stats>().gameObject.SetActive(false);


    }

    public void Restart() {
        SceneManager.LoadScene(0);
    }

    public void ToggleGore() {
        goreEnabled = !goreEnabled;
        GameObject.Find("Gore").GetComponentInChildren<Text>().text = goreEnabled ? "Disable\nGore" : "Enable\nGore";
    }

    public void ToggleCasings() {
        casingsEnabled = !casingsEnabled;
        GameObject.Find("Casings").GetComponentInChildren<Text>().text = casingsEnabled ? "Disable\nCasings" : "Enable\nCasings";
    }
}

