﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
Still need to correct the issue where, when the cursor is closer to the player, 
the weapon's shots are less accurate than when it is far away. One solution, is to raycast in the direction of the cursor, 
but take the position of a raycasthit with an invisible barrier that surrounds the playarea, and add the accuracy modifier to 
that position. This way, the proximity of the cursor will be irrelevant.
*/
public class WeaponController : MonoBehaviour {
    private GameManager gameManager;

    private float timeSinceLastFrame;

    public int maxUpgrades;
    public int unlockedUpgrades;

    private AudioManager audioManager;

    public float myVolume = .3f;
    private GameObject chamber;

    public GameObject bulletCasingPrefab;
    private GameObject casing;

    public float damage;
    public bool isAutomatic = true;
    public float numProjectiles = 1;
    public bool isUnlocked = false;
    public float bulletSpeed;
    public float reloadTime;
    public int capacity;
    public float delayBase;
    public float accuracyModMax;
    public float accuracyModMin;


    public AudioClip shotSound;
    public GameObject bulletPrefab;
    private GameObject newBullet;
    public GameObject reloadFill;
    public SpriteRenderer reloadOutline;

    private Vector3 reloadFillFull;
    private Vector3 reloadFillTemp;

    private Vector2 target;
    private Vector2 myPos;
    private Vector2 direction;
    private Vector2 projectileTarget;
    private Vector2 projectileDirection;

    private Quaternion rotation;

    private Vector3 scaleTemp;

    //Generic variables
    
    

    public Sprite icon;
    public Sprite ammoIcon;
    public WeaponUI weaponUI;
    public Text weaponText;

    private bool isReloading = false;

    //Reload Variables
    
    public float elapsedReloadTime = 0;

    //Capacity Variables
    
    public int roundsLoaded;

    //Firerate Variables
    
    public float delay;

    //Accuracy Variables
    
    public float accuracyMod;

    private List<GameObject> bullets;
    public string caliber = ".22";

    public Stats stats;

    private Motorbike motorbike;


    public float cursorMaxXDist = 17.5f;
    public float cursorMaxYDist = 9.5f;

    public float cursorXDist;
    public float cursorYDist;

    bool hasFiredThisFrame = false;

    void Start() {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        audioManager = GameObject.FindObjectOfType<AudioManager>();
        chamber = GameObject.Find(gameObject.name + "/Chamber");

        if(numProjectiles > 1) {
            myVolume *= 5;
        }
        motorbike = GameObject.FindObjectOfType<Motorbike>();
        delay = 0;
        if (!PlayerPrefs.HasKey(name + "roundsLoaded")) {
            roundsLoaded = capacity;
        }
        reloadFill = GameObject.Find("ReloadFill");
        reloadOutline = GameObject.Find("ReloadOutline").GetComponent<SpriteRenderer>();
        reloadFillFull = new Vector3(1.674579f, 1.674579f, 1.674579f);
        reloadFillTemp = reloadFillFull;
        reloadFillTemp.x = 0;
        reloadFill.transform.localScale = reloadFillTemp;
        reloadOutline.enabled = false;
        bullets = new List<GameObject>();
        weaponUI = GameObject.FindObjectOfType<WeaponUI>();
        weaponText = GameObject.Find("WeaponText").GetComponent<Text>();
        stats = GetComponentInParent<Stats>();
    }

    void Update() {
        hasFiredThisFrame = false;

        timeSinceLastFrame = Time.deltaTime;
        int numShots = Mathf.CeilToInt(timeSinceLastFrame / delayBase);

        if (Time.deltaTime > 0) {
            RotateArm();
            ScaleArm();
        }

        if (numProjectiles == 1) {
            if (isAutomatic) {
                for (int i = 0; i < numShots; i++) {
                    Fire();
                }
            } else {
                Fire();
            }
        } else if (isAutomatic) {
            if (isAutomatic) {
                for (int i = 0; i < numShots; i++) {
                    FireMulti();
                }
            }
        } else {
            FireMulti();
        }

        if (isReloading || Input.GetKeyDown(KeyCode.R)) {
            Reload();
        }

    }

    private float GetCursorXDist() {
        if (stats.transform.position.x > target.x) {
            return stats.transform.position.x - target.x;
        } else {
            return target.x - stats.transform.position.x;
        }
    }

    private float GetCursorYDist() {
        if (stats.transform.position.y > target.y) {
            return stats.transform.position.y - target.y;
        } else {
            return target.y - stats.transform.position.y;
        }
    }

    void RotateArm() {
        //17.5 max X dist
        //9.5 max y dist
        target = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));       
        
        myPos = new Vector2(transform.position.x, transform.position.y);
        direction = target - myPos;
        direction.Normalize();
        rotation = Quaternion.Euler(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg);
        transform.rotation = rotation;
    }

    void ScaleArm() {
        scaleTemp = transform.localScale;

        if (transform.eulerAngles.z > 270 || transform.eulerAngles.z < 90) {
            scaleTemp.y = 1;
        } else {
            scaleTemp.y = -1;
        }

        transform.localScale = scaleTemp;
    }

    float GetNewAccuracyMod() {
        accuracyMod = Random.Range(accuracyModMin, accuracyModMax) * (1 - stats.accuracyModifier);
        return accuracyMod;
    }

    bool CanFire() {

        bool temp = true;

        if (GameObject.Find("ShopUI") != null) {
            temp = false;
        }

        if (GameObject.Find("WeaponUI") != null) {
            temp = false;
        }

        if (GameObject.Find("AbilitiesUI") != null) {
            temp = false;
        }

        if (GameObject.Find("AchievementsUI") != null) {
            temp = false;
        }

        return delay <= 0 && roundsLoaded > 0 && temp && !isReloading && !stats.GetIsOverUI();
    }    

    void Fire() {

        cursorXDist = GetCursorXDist();
        cursorYDist = GetCursorYDist();

        float xPercent = cursorXDist / cursorMaxXDist;
        float yPercent = cursorYDist / cursorMaxYDist;

        //Determine Projectile velocity
        projectileTarget = new Vector2(target.x + (GetNewAccuracyMod() * yPercent), 
            target.y + (GetNewAccuracyMod() * xPercent));
        projectileDirection = projectileTarget - myPos;
        projectileDirection.Normalize();

        if (newBullet != null) {
            newBullet.GetComponent<Rigidbody2D>().AddForce(projectileDirection * bulletSpeed);
            newBullet = null;
        }

        if ((Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)) && roundsLoaded <= 0 && elapsedReloadTime == 0) {
            Reload();

        } else if (isAutomatic && Input.GetMouseButton(0) && CanFire()) {

            CreateCasing();
            motorbike.StopRepairing();
            roundsLoaded--;
            newBullet = (GameObject)Instantiate(bulletPrefab, GameObject.Find("Barrel").transform.position, transform.rotation);
            delay = delayBase;
            audioManager.PlayClip(shotSound);

        } else if (Input.GetMouseButtonDown(0) && CanFire()) {
            CreateCasing();
            motorbike.StopRepairing();
            roundsLoaded--;
            newBullet = (GameObject)Instantiate(bulletPrefab, GameObject.Find("Barrel").transform.position, transform.rotation);
            audioManager.PlayClip(shotSound);
            delay = delayBase;

        } else {
            delay -= Time.deltaTime;
        }

        weaponUI.UpdateUI(this);

        
    }

    private void CreateCasing() {
        if (gameManager.casingsEnabled && !hasFiredThisFrame) {
            hasFiredThisFrame = true;
            casing = (GameObject)Instantiate(bulletCasingPrefab, chamber.transform.position, Quaternion.identity, this.transform);
        }
    }

    void FireMulti() {

        cursorXDist = GetCursorXDist();
        cursorYDist = GetCursorYDist();

        float xPercent = cursorXDist / cursorMaxXDist;
        float yPercent = cursorYDist / cursorMaxYDist;

        foreach (GameObject bullet in bullets) {
            projectileTarget = new Vector2(target.x + (GetNewAccuracyMod() * yPercent),
            target.y + (GetNewAccuracyMod() * xPercent));
            projectileDirection = projectileTarget - myPos;
            projectileDirection.Normalize();
            bullet.GetComponent<Rigidbody2D>().AddForce(projectileDirection * bulletSpeed);
        }



        bullets.Clear();

        if ((Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)) && roundsLoaded <= 0 && elapsedReloadTime == 0) {
            Reload();
        } else if (isAutomatic && Input.GetMouseButton(0) && CanFire()) {
            CreateCasing();
            motorbike.StopRepairing();
            roundsLoaded--;
            for (int i = 0; i < numProjectiles; i++) {
                bullets.Add((GameObject)Instantiate(bulletPrefab, GameObject.Find("Barrel").transform.position, transform.rotation));
                delay = delayBase;
            }
            audioManager.PlayClip(shotSound);
        } else if (Input.GetMouseButtonDown(0) && CanFire()) {
            CreateCasing();
            motorbike.StopRepairing();
            roundsLoaded--;
            for (int i = 0; i < numProjectiles; i++) {
                bullets.Add((GameObject)Instantiate(bulletPrefab, GameObject.Find("Barrel").transform.position, transform.rotation));
                delay = delayBase;                
            }
            audioManager.PlayClip(shotSound);
        } else {
            delay -= Time.deltaTime;
        }

        weaponUI.UpdateUI(this);
    }

    void Reload() {
        WeaponChanger weaponChanger = GameObject.FindObjectOfType<WeaponChanger>();
        WeaponController currentWeapon = weaponChanger.weapons[weaponChanger.currWeapon];

        if (stats.GetRoundsOwned(caliber) > 0 && currentWeapon.roundsLoaded < currentWeapon.capacity) {
            reloadOutline.enabled = true;
            isReloading = true;
            if (elapsedReloadTime >= reloadTime) {
                int roundsTemp = (int)roundsLoaded;
                roundsLoaded = stats.GetRoundsOwned(caliber) >= capacity ? capacity : roundsLoaded + stats.GetRoundsOwned(caliber);
                for (int i = 0; i < roundsLoaded - roundsTemp; i++) {
                    stats.UseAmmo(caliber);
                }

                isReloading = false;
                elapsedReloadTime = 0;
                reloadOutline.enabled = false;
            } else {
                elapsedReloadTime += Time.deltaTime * stats.reloadModifier;
            }
            reloadFill.transform.localScale = GetScale();
        }
    }

    Vector3 GetScale() {
        reloadFillTemp.x = reloadFillFull.x * (elapsedReloadTime / reloadTime);
        return reloadFillTemp;
    }

    public override string ToString() {
        return caliber;
    }

}