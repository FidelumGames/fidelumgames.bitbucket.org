﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpgradeButton : MonoBehaviour {

    public int maxLevel = 5;
    public int currLevel = 0;
    public int[] costs;
    public float[] levelValues;
    public string upgrade = "Damage";

    private UIManager uiManager;
    private Shop shop;

    private List<WeaponController> weapons;
    public WeaponController myWeapon;
    public string myDescription;


    void Start() {
        uiManager = GameObject.FindObjectOfType<UIManager>();
        shop = GameObject.FindObjectOfType<Shop>();
        weapons = GameObject.FindObjectOfType<WeaponChanger>().weapons;
        SetMyWeapon();
    }

    private void SetMyWeapon() {
        foreach (WeaponController weapon in weapons) {
            if (weapon.transform.name == transform.parent.name.Substring(0, transform.parent.name.Length - 12)) {
               
                myWeapon = weapon;
                return;
            }
        }
       // Debug.LogError(transform.name + " upgrade button has no weapon assigned to it!");
    }



    public void BuyMe() {
        shop.BuyUpgrade(this);
        ShowMyInfo();
    }

    public void MakeMeActiveButton() {
        uiManager.SetActiveUpgradeButton(this);
    }

    public void ShowMyInfo() {

        string rateOfFire = myWeapon.transform.name == "9mm" ? "10" : string.Format("{0:F1}", 1.0f / (float)myWeapon.delayBase);
        string accuracy = string.Format("{0:F0}", ((5f - myWeapon.accuracyModMax) / 5f) * 100);

        myDescription = "Damage: " + myWeapon.damage +
                                "\nRate Of Fire: " + rateOfFire + " rounds/s" +
                                "\nReload Time: " + string.Format("{0:F1}", myWeapon.reloadTime) + " seconds" +
                                "\nCapacity: " + myWeapon.capacity +
                                "\nAccuracy: %" + accuracy +
                                "\nNumber Of Projectiles: " + myWeapon.numProjectiles;
        uiManager.ShowUpgradeInfoPanel();
        
    }

    public string GetCurrentStatValueAsString() {

        switch (gameObject.name) {
            case "Damage":
                return myWeapon.damage + "";                
            case "Accuracy":
                float accuracy = myWeapon.accuracyModMax;
                return "%" + string.Format("{0:F0}", ((5f - accuracy) / 5f) * 100);
            case "Reload Time":
                float reloadTime = myWeapon.reloadTime;
                return string.Format("{0:F1}", reloadTime);
            case "Capacity":
                return myWeapon.capacity + "";
            case "Fire Rate":
                return myWeapon.transform.name == "9mm" ? "10" : string.Format("{0:F1}", 1.0f / (float)myWeapon.delayBase);
            case "Projectiles":
                return myWeapon.numProjectiles + "";
        }

        return "Error";
    }

    public string GetNextStatValueAsString() {

        switch (gameObject.name) {
            case "Damage":
                return levelValues[currLevel] + "";
            case "Accuracy":
                float accuracy = levelValues[currLevel];
                return "%" + string.Format("{0:F0}", ((5f - accuracy) / 5f) * 100);
            case "Reload Time":
                float reloadTime = levelValues[currLevel];
                return string.Format("{0:F1}", reloadTime);
            case "Capacity":
                return levelValues[currLevel] + "";
            case "Fire Rate":
                return myWeapon.transform.name == "9mm" ? "10" : string.Format("{0:F1}", 1.0f / (float)levelValues[currLevel]);
            case "Projectiles":
                return levelValues[currLevel] + "";
        }

        return "Error";
    }
}
