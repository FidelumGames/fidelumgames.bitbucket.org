﻿using UnityEngine;
using System.Collections;

public class Crow : MonoBehaviour {

    private AchievementManager achievementManager;

    public enum CrowType {
        Crow, Bloodbird
    };

    public CrowType crowType;

    public GameObject deathPrefab;

    private Animator animator;
    public Animator pixelAnimator;
    private Stats player;
    public bool movingLeft = true;
    private Stats target; 

    public float distanceToStartDive;
    public float moveSpeed;
    public float xMax, xMin;
    public float damage;
    public float health;
    public float dist;
    private float attackDelay = 0f;
    public float delayBase = .25f;
    public DropItem[] dropItems;

    public int XP = 10;

    public GameObject pickupText;
    private PickupText newPickupText;

    private Vector3 scaleTemp;
    public bool isAttacking = false;

    public AudioClip[] enemySounds;
    private AudioSource aSource;

    public float soundDelayMin;
    public float soundDelayMax;
    private float timeToNextSound;

    private AudioManager audioManager;

    public float healthFactor;
    private SpawnerManager spawnerManager;

    void Start () {
        spawnerManager = GameObject.FindObjectOfType<SpawnerManager>();
        audioManager = GameObject.FindObjectOfType<AudioManager>();
        achievementManager = GameObject.FindObjectOfType<AchievementManager>();
        player = GameObject.FindObjectOfType<Stats>();
        animator = GetComponent<Animator>();
        aSource = GetComponent<AudioSource>();
        foreach(Animator animatorChild in GetComponentsInChildren<Animator>()) {
            if (! (animatorChild.transform == this.transform)) {
                pixelAnimator = animatorChild;
                break;
            }
        }

        scaleTemp = transform.localScale;
        SetNextEnemySound();
        IncreaseHealth();
    }

    private void IncreaseHealth() {
        health += spawnerManager.currentWave * healthFactor;
    }
    private void SetNextEnemySound() {
        int index = Random.Range(0, enemySounds.Length);
        audioManager.PlayClip(enemySounds[index]);
        
    }


    void Update () {
        if (!isAttacking) {
            Move();             
        } else {
            pixelAnimator.SetBool("dive", false);
            scaleTemp.x = 1;
            if (player) {
                transform.position = player.transform.position;
            }
            if (!target) {
                target = GameObject.FindObjectOfType<Stats>();
            }
            Attack();
        }

        transform.localScale = scaleTemp;
        CountDownToEnemySound();
    }

    private void CountDownToEnemySound() {
        if (timeToNextSound <= 0) {
            timeToNextSound = Random.Range(soundDelayMin, soundDelayMax);
            SetNextEnemySound();

        } else {
            timeToNextSound -= Time.deltaTime;
        }
    }
    void Move() {
        HandleDiveCondition();
        if (movingLeft) {
            scaleTemp.x = 1;
            transform.Translate(Vector2.left * moveSpeed * Time.deltaTime);
            if(transform.position.x <= xMin) {
                movingLeft = false;
            }
        } else {
            scaleTemp.x = -1;
            transform.Translate(Vector2.left * moveSpeed * Time.deltaTime);
            if(transform.position.x >= xMax) {
                movingLeft = true;
            }
        }
    }

    void Attack() {
        if (target && attackDelay <= 0) {
            if (this.crowType == CrowType.Bloodbird) {
                target.AddBloodBird(this);
            }
            target.TakeDamage(damage);
            attackDelay = delayBase;
        }

        attackDelay -= Time.deltaTime;
    }

    public void TakeDamage(float damage) {
        health -= damage;
        newPickupText = ((GameObject)Instantiate(pickupText, (Vector2)pixelAnimator.transform.position,
            Quaternion.identity)).GetComponent<PickupText>();
        newPickupText.setProperties("-" + (int)damage, Color.white);
        if (health <= 0) {
            Die();
        }
    }

    void Die() {
        achievementManager.IncreaseKills();
        target = player.GetComponent<Stats>();
        

        target.GainXP(XP);
        newPickupText = ((GameObject)Instantiate(pickupText, (Vector2)pixelAnimator.transform.position,
            Quaternion.identity)).GetComponent<PickupText>();
        newPickupText.setProperties("+" + (int)target.GetTotalXPGainedInt(XP) + " XP", Color.blue);
        DropItem();
        if (this.crowType == CrowType.Bloodbird) {
            target.RemoveBloodBird(this);
        }

        Instantiate(deathPrefab, GetComponentInChildren<CrowCollisionHandler>().transform.position, GetComponentInChildren<CrowCollisionHandler>().transform.rotation);
        Destroy(gameObject);
    }


    public void EndDie() {
        Instantiate(deathPrefab, GetComponentInChildren<CrowCollisionHandler>().transform.position, GetComponentInChildren<CrowCollisionHandler>().transform.rotation);
        Destroy(gameObject);
    }

    void HandleDiveCondition() {

        if (animator.GetBool("dive")) {
            animator.ResetTrigger("dive");
            pixelAnimator.SetBool("dive", false);
        }

        if (player) {
            dist = Mathf.Abs(player.transform.position.x - transform.position.x);
            if (transform.position.x < player.transform.position.x && !movingLeft) {

                if (Mathf.Abs(player.transform.position.x - transform.position.x) <= distanceToStartDive && !isAttacking) {
                    if (!animator.GetBool("dive")) {
                        animator.SetTrigger("dive");
                        pixelAnimator.SetBool("dive", true);
                    }
                }
            } else if (transform.position.x > player.transform.position.x && movingLeft) {

                dist = Mathf.Abs(transform.position.x - player.transform.position.x);
                if (Mathf.Abs(transform.position.x - player.transform.position.x) <= distanceToStartDive && !isAttacking) {
                    animator.SetTrigger("dive");
                    pixelAnimator.SetBool("dive", true);
                }
            }
        }
    }

    private void DropItem() {
        float roll = Random.Range(0, 100);

        for (int i = 0; i < dropItems.Length; i++) {
            if (roll <= dropItems[i].chance) {
                Instantiate((GameObject)dropItems[i].dropItem, pixelAnimator.transform.position, 
                    Quaternion.identity);
                return;
            }
        }
    }


}
