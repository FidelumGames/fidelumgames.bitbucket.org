﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Motorbike : MonoBehaviour {

    public bool canRepair = false;
    public bool isRepairing = false;

    public GameObject soundPrefab;

    private bool isRepaired = false;

    public float healthRepaired = 0f;
    public float totalHealth = 100;
    public float repairDelay = 0;
    public float delayBase = 1f;

    public PickupText pickupText;

    public GameObject reloadFill;
    public SpriteRenderer reloadOutline;
    private Vector3 reloadFillFull; //Scale of fill bar when full
    private Vector3 reloadFillTemp; //Used in manipulating the reloadFill's localScale
    private Vector3 initialFill;
    

    private AchievementManager achievementManager;

    public Stats player;

    public GameObject pickupTextPrefab;

    public GameObject ridePrompt;


    void Start() {
        achievementManager = GameObject.FindObjectOfType<AchievementManager>();
        player = GameObject.FindObjectOfType<Stats>();
        FillInit();
    }

    void FillInit() {
        reloadFill = GameObject.Find("BikeReloadFill");
        reloadOutline = GameObject.Find("BikeReloadOutline").GetComponent<SpriteRenderer>();
        reloadFillFull = reloadFill.transform.localScale;
        reloadFillTemp = reloadFillFull;
        reloadFillTemp.x = 0;
        reloadFill.transform.localScale = reloadFillTemp;
        initialFill = reloadFillTemp;
        reloadOutline.enabled = false;
    }

    void Update() {

        if(isRepaired && Input.GetKeyDown(KeyCode.Q) && ridePrompt.activeSelf) {
            GameObject.Find("IntroObjects").GetComponent<Animator>().SetBool("Win", true);
            Destroy(ridePrompt);
            GameObject.FindObjectOfType<GameManager>().Win();
        }


        if (!canRepair) {
            HideFillBar();

        } else {
            reloadFill.transform.localScale = GetScale();
        }

        repairDelay -= Time.deltaTime;

        if (isRepairing && !isRepaired) {
            if (Input.anyKeyDown) {
                StopRepairing();
            }

            if (repairDelay <= 0) {
                Repair();
                Instantiate(soundPrefab);
            }
        }

        if (Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S)) {
            StopRepairing();
        }

        if (canRepair && (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)) && !isRepaired) {
            isRepairing = true;
        }
    }

    void HideFillBar() {
        reloadFill.transform.localScale = initialFill;
    }

    Vector3 GetScale() {
        reloadFillTemp.x = reloadFillFull.x * (healthRepaired / totalHealth);
        return reloadFillTemp;
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "Player") {
            if (!isRepaired) {
                canRepair = true;
                reloadOutline.enabled = true;
                reloadFill.transform.localScale = GetScale();
            } else {
                if (ridePrompt) {
                    ridePrompt.SetActive(true);
                }
                reloadOutline.enabled = false;
                reloadFill.SetActive(false);
            }
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.tag == "Player") {
            DisableRepair();
            ridePrompt.SetActive(false);
        }
    }

    public void StopRepairing() {
        isRepairing = false;
    }

    public void DisableRepair() {
        canRepair = false;
        reloadOutline.enabled = false;
        reloadFill.transform.localScale = new Vector3(0, 1.674579f, 1.674579f);
    }

    private void Repair() {
        achievementManager.Repair(this);
        repairDelay = delayBase;
        healthRepaired += player.GetRepairAmount();
        pickupText = ((GameObject)Instantiate(pickupTextPrefab, (Vector2)transform.position, Quaternion.identity)).GetComponent<PickupText>();
        pickupText.setProperties("+" + player.GetRepairAmount(), Color.yellow);
        if (healthRepaired >= totalHealth) {
            healthRepaired = totalHealth;
            isRepaired = true;
            reloadOutline.enabled = false;
            reloadFill.SetActive(false);
            ridePrompt.SetActive(true);
        }
    }
}
