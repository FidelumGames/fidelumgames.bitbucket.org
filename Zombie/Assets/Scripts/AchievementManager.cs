﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AchievementManager : MonoBehaviour {

    private Stats stats;
    public List<AchievementButton> achievements = new List<AchievementButton>();
    private List<AchievementButton> achievementQueue = new List<AchievementButton>();
    public List<WeaponController> weapons = new List<WeaponController>();

    private int numWeapons = 11;
    public int unlockedWeapons = 1;

    public int maxAbilities = 152;
    public int upgradedAbilities = 0;

    public int numKills = 0;
    public int kills1, kills2, kills3, kills4, kills5;
    public int killsUnlocked = 0;

    public int numLimbs, limbs1, limbs2, limbs3;
    public int limbsUnlocked = 0;

    public float damageTaken, damageTaken1, damageTaken2, damageTaken3 = 0;
    public int damageTakenUnlocked = 0;

    public float explodedEnemies, explode1, explode2, explode3 = 0;
    public float explodesUnlocked = 0;

    public int repairUnlocked = 0;

    public int loadedAmount = 5000;
    public int earnedCash = 0;
    public bool loadedUnlocked = false;

    public bool invulnerableUnlocked = false;
    public bool cuttingItCloseUnlocked = false;

    public int wavesUnlocked = 0;

    public void UnlockWavesCompleted(int waveNumber) {
        if (waveNumber >= 70 && wavesUnlocked == 7) {
            UnlockAchievement("To Infinity... And Beyond!");
            wavesUnlocked++;
            stats.AddCash(2000);
        } else if (waveNumber >= 60 && wavesUnlocked == 6) {
            UnlockAchievement("What Is 1% Of Infinity Anyway?");
            wavesUnlocked++;
            stats.AddCash(1500);
        } else if (waveNumber >= 50 && wavesUnlocked == 5) {
            UnlockAchievement("Now It's 1%. (Again, Kidding)");
            wavesUnlocked++;
            stats.AddCash(1000);
        } else if (waveNumber >= 40 && wavesUnlocked == 4) {
            UnlockAchievement("1% Complete. (Just Kidding)");
            wavesUnlocked++;
            stats.AddCash(600);
        } else if (waveNumber >= 30 && wavesUnlocked == 3) {
            UnlockAchievement("Are We There Yet?");
            wavesUnlocked++;
            stats.AddCash(400);
        } else if (waveNumber >= 20 && wavesUnlocked == 2) {
            UnlockAchievement("Not Even Close");
            wavesUnlocked++;
            stats.AddCash(200);
        } else if (waveNumber >= 10 && wavesUnlocked == 1) {
            UnlockAchievement("Scratched The Surface");
            wavesUnlocked++;
            stats.AddCash(100);
        } else if (waveNumber >= 1 && wavesUnlocked == 0) {
            UnlockAchievement("Good For You");
            wavesUnlocked++;
            stats.AddCash(50);
        }
    }

    public void UnlockFightAnotherDay() {
        UnlockAchievement("Fight Another Day...");      
    }

    public void UnlockInvulnerable() {
        if (!invulnerableUnlocked) {
            invulnerableUnlocked = true;
            stats.damageResistance -= .1f;
            UnlockAchievement("Invulnerable");
        }
    }

    public void UnlockCuttingItClose() {
        if (!cuttingItCloseUnlocked) {
            cuttingItCloseUnlocked = true;
            stats.damageResistance -= .1f;
            UnlockAchievement("Cutting It Close");
        }
    }

    public void IncreaseEarnedCash(int amount) {
        earnedCash += amount;
        if (earnedCash >= loadedAmount && !loadedUnlocked) {
            loadedUnlocked = true;
            UnlockAchievement("Loaded");
            stats.moneyMultiplier += .5f;
        }
    }

    public void Repair(Motorbike motorbike) {
        float percent = motorbike.healthRepaired / motorbike.totalHealth * 100;

        if (percent >= 75 && repairUnlocked == 2) {
            repairUnlocked++;
            stats.repairModifier++;
            UnlockAchievement("Almost");
        } else if (percent >= 50 && repairUnlocked == 1) {
            repairUnlocked++;
            stats.repairModifier++;
            UnlockAchievement("Grease Monkey");
        } else if (percent >= 25 && repairUnlocked == 0) {
            repairUnlocked++;
            stats.repairModifier++;
            UnlockAchievement("Daryl");
        }

    }

    // Use this for initialization
    void Start() {
        weapons = GameObject.FindObjectOfType<WeaponChanger>().weapons;
        stats = GameObject.FindObjectOfType<Stats>();

       
       // achievements = GameObject.FindObjectsOfType<AchievementButton>().ToList();
    }

    // Update is called once per frame
    void Update() {
        ExecuteUnlock();
    }

    public void IncreaseExplodes() {
        explodedEnemies++;
        if (explodedEnemies >= explode3 && explodesUnlocked == 2) {
            explodesUnlocked++;
            UnlockAchievement("I Need A Shower");
            stats.damageModifier = 1.75f;
        } else if (explodedEnemies >= explode2 && explodesUnlocked == 1) {
            explodesUnlocked++;
            UnlockAchievement("Guts. Guts Everywhere");
            stats.damageModifier = 1.5f;
        } else if (explodedEnemies >= explode1 && explodesUnlocked == 0) {
            explodesUnlocked++;
            UnlockAchievement("Where'd They Go?");
            stats.damageModifier = 1.25f;
        }
    }

    public void UnlockSupporter() {
        stats.AddCash(100);
        UnlockAchievement("Supporter");
        Application.ExternalEval("window.open(\"https://www.youtube.com/channel/UC4_myf9_OFoVqoP4KvH8D7g\")");
    }

    public void UnlockFriend() {
        stats.skillPoints++;
        UnlockAchievement("Friend");
        Application.ExternalEval("window.open(\"http://www.facebook.com/Fidelum-Games-528427267362770/\")");
    }

    private void IncreaseWeaponStats(WeaponController weapon) {
        weapon.damage++;
        weapon.reloadTime *= .75f;
        weapon.capacity = (int)(weapon.capacity * 1.25f);
        weapon.accuracyModMin *= .75f;
        weapon.accuracyModMax *= .75f;
        weapon.delayBase *= .75f;
        if (weapon.numProjectiles > 1) {
            weapon.numProjectiles++;
        }
    }

    private void UnlockWeaponUpgradeAchievement(WeaponController weapon) {
        IncreaseWeaponStats(weapon);

        switch (weapon.name) {
            case "Revolver":
                UnlockAchievement("Cowboy");
                break;
            case "9mm":
                UnlockAchievement("Secret Agent");
                break;
            case "Uzi":
                UnlockAchievement("Hornet");
                break;
            case "Shotgun":
                UnlockAchievement("Crow Hunter");
                break;
            case "MP5":
                UnlockAchievement("Wasp Swarm");
                break;
            case "AK47":
                UnlockAchievement("Warlord");
                break;
            case "Combat Shotgun":
                UnlockAchievement("Door Kicker");
                break;
            case "Sniper":
                UnlockAchievement("Sharp Shooter");
                break;
            case "M4A1":
                UnlockAchievement("Soldier");
                break;
            case "AA12":
                UnlockAchievement("Mush Maker");
                break;
            case "Minigun":
                UnlockAchievement("GOD");
                break;
            default:
              //  Debug.LogWarning("No weapon by name " + weapon.name +
              //      " exists. AhievementManager.UnlockWeaponUpgradeAchievement");
                break;
        }
    }

    public void BuyWeapon() {
        unlockedWeapons++;
        if (unlockedWeapons >= numWeapons) {
            UnlockAchievement("Duke Blazkowicz");
            stats.AddAmmo(".22", 200);
            stats.AddAmmo("12 Ga.", 200);
            stats.AddAmmo(".50", 200);
            stats.AddAmmo("7.62", 200);
            stats.AddAmmo("5.56", 200);
            stats.AddAmmo("5mm", 200);
            stats.AddAmmo("10mm", 200);
            stats.AddAmmo("9mm", 200);
        }
    }

    public void UpgradeAbility() {
        upgradedAbilities++;
        if (upgradedAbilities >= maxAbilities) {
            UnlockAchievement("Unstoppable");
            stats.maxJumps++;
            stats.repairModifier++;
            stats.runSpeed++;
            stats.XPMultiplier = 3;
            stats.damageResistance = .4f;
            stats.moneyMultiplier = 3;
            stats.healthMax += 50;
            stats.magnetStrength = 12;
            stats.regenAmount++;
            stats.reloadModifier = 3;
            stats.accuracyModifier = .7f;
        }
    }

    private void UnlockAchievement(string name) {
     //   Debug.Log("UnlockAchievment");
        foreach (AchievementButton achievement in achievements) {
            if (achievement.name == name) {
                achievementQueue.Add(achievement);               
                break;
            }
        //    Debug.LogWarning("No achievement by name " + name +
       //           " exists. AhievementManager.UnlockAchievement");
        }
    }

    private void ExecuteUnlock() {
        if(achievementQueue.Count > 0 && GameObject.FindObjectOfType<AchievementBox>() == null) {
            if (achievementQueue[0].IsUnlocked != 1) {
                achievementQueue[0].Unlock();
                if(achievementQueue[0].name == "Fight Another Day...") {
                    GameObject.FindObjectOfType<SaveSystem>().Save();
                }
            }
            achievementQueue.RemoveAt(0);
        }
    }

    public string GetProgressText(string name) {
        foreach (AchievementButton achievement in achievements) {
            if (achievement.name == name) {
                switch (achievement.achievementType) {
                    case AchievementButton.AchievementType.WeaponUpgrade:
                        return GetWeaponProgressText(name);
                    default:
                        switch (name) {
                            case "Duke Blazkowicz":
                                return unlockedWeapons + " / " + numWeapons;
                            case "Unstoppable":
                                return upgradedAbilities + " / " + maxAbilities;
                            case "Wow... Not":
                                return numKills + " / " + kills1;
                            case "A Good Start":
                                return numKills + " / " + kills2;
                            case "What Stinks?":
                                return numKills + " / " + kills3;
                            case "Maniac":
                                return numKills + " / " + kills4;
                            case "ZomBane":
                                return numKills + " / " + kills5;
                            case "Bruised":
                                return damageTaken + " / " + damageTaken1;
                            case "Beaten":
                                return damageTaken + " / " + damageTaken2;
                            case "How Are You Still Walking?":
                                return damageTaken + " / " + damageTaken3;
                            case "Butcher":
                                return numLimbs + " / " + limbs1;
                            case "Cruel And Unusual":
                                return numLimbs + " / " + limbs2;
                            case "Look, It's Meat Mountain!":
                                return numLimbs + " / " + limbs3;
                            case "Where'd They Go?":
                                return explodedEnemies + " / " + explode1;
                            case "Guts. Guts Everywhere":
                                return explodedEnemies + " / " + explode2;
                            case "I Need A Shower":
                                return explodedEnemies + " / " + explode3;
                            case "Loaded":
                                return earnedCash + " / " + loadedAmount;
                            default:
                                return "";
                        }
                }
            }
        }
        return "";
    }

    public string GetWeaponProgressText(string name) {

        string weaponName = "";

        switch (name) {
            case "Cowboy":
                weaponName = "Revolver";
                break;
            case "Mush Maker":
                weaponName = "AA12";
                break;
            case "Door Kicker":
                weaponName = "Combat Shotgun";
                break;
            case "Crow Hunter":
                weaponName = "Shotgun";
                break;
            case "Secret Agent":
                weaponName = "9mm";
                break;
            case "GOD":
                weaponName = "Minigun";
                break;
            case "Wasp Swarm":
                weaponName = "Uzi";
                break;
            case "Hornet":
                weaponName = "MP5";
                break;
            case "Sharp Shooter":
                weaponName = "Sniper";
                break;
            case "Soldier":
                weaponName = "M4A1";
                break;
            case "Warlord":
                weaponName = "AK47";
                break;
        }
        foreach (WeaponController weapon in weapons) {
            if (weapon.name == weaponName) {
                return weapon.unlockedUpgrades + " / " + weapon.maxUpgrades;
            }
        }

        return "Error";
    }

    public void UpgradeWeapon(WeaponController weapon) {
        weapon.unlockedUpgrades++;
        if (weapon.unlockedUpgrades >= weapon.maxUpgrades) {
            UnlockWeaponUpgradeAchievement(weapon);
        }
    }

    public void IncreaseKills() {
        numKills++;
        if (numKills >= kills5 && killsUnlocked == 4) {
            killsUnlocked++;
            UnlockAchievement("ZomBane");
            stats.AddCash(2000);
        } else if (numKills >= kills4 && killsUnlocked == 3) {
            killsUnlocked++;
            UnlockAchievement("Maniac");
            stats.AddCash(1000);
        } else if (numKills >= kills3 && killsUnlocked == 2) {
            killsUnlocked++;
            UnlockAchievement("What Stinks?");
            stats.AddCash(500);
        } else if (numKills >= kills2 && killsUnlocked == 1) {
            killsUnlocked++;
            UnlockAchievement("A Good Start");
            stats.AddCash(250);
        } else if (numKills >= kills1 && killsUnlocked == 0) {
            killsUnlocked++;
            UnlockAchievement("Wow... Not");
            stats.AddCash(50);
        }
    }

    public void TakeDamage(float damage) {
        damageTaken += damage;

        if (damageTaken >= damageTaken3 && damageTakenUnlocked == 2) {
            damageTakenUnlocked++;
            UnlockAchievement("How Are You Still Walking?");
            stats.healthMax += 100;
        } else if (damageTaken >= damageTaken2 && damageTakenUnlocked == 1) {
            damageTakenUnlocked++;
            UnlockAchievement("Beaten");
            stats.healthMax += 50;
        } else if (damageTaken >= damageTaken1 && damageTakenUnlocked == 0) {
            damageTakenUnlocked++;
            UnlockAchievement("Bruised");
            stats.healthMax += 50;
        }
    }

    public void RemoveLimb() {
        numLimbs++;

        if (numLimbs >= limbs3 && limbsUnlocked == 2) {
            limbsUnlocked++;
            stats.accuracyModifier += .1f;
            UnlockAchievement("Look, It's Meat Mountain!");
        } else if (numLimbs >= limbs2 && limbsUnlocked == 1) {
            limbsUnlocked++;
            stats.accuracyModifier += .1f;
            UnlockAchievement("Cruel And Unusual");
        }
        if (numLimbs >= limbs1 && limbsUnlocked == 0) {
            limbsUnlocked++;
            stats.accuracyModifier += .1f;
            UnlockAchievement("Butcher");
        }
    }
}