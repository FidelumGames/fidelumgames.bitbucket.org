﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Stats : MonoBehaviour {

    public GameObject headPrefab, legPrefab, armPrefab, torsoPrefab;
    public GameObject headPosition, rightLegPosition, leftLegPosition, rightArmPosition, leftArmPosition, torsoPosition;
    public GameObject restart;
    public int maxJumps = 3;

    public List<AudioClip> painSounds = new List<AudioClip>();
    private float painTimer = 0f;
    public float painTimerMin, painTimerMax;

    public int cash;
    public float moneyMultiplier = 1f;
    public float damageResistance = 1f;
    public float regenAmount = 1f;
    private float regenTimer = 1f;
    public float reloadModifier = 1f;
    public float accuracyModifier = 1f;

    public float damageModifier = 1;

    private PlayerController playerController;

    public int skillPoints = 0;
    public int maxLevel = 50;

    public float healthGainMultiplier = 1.5f;

    public float ammoMultiplier = 1.5f;

    public float magnetStrength = 10f;

    public float runSpeed = 10f;

    public float repair = 1f;
    public float repairModifier = 1f;

    public float health = 100;
    public float healthMax = 100;

    private float lastLevelXP;

    public int playerAttackDamage;

    public void SetDamage(int dam) {
        playerAttackDamage = dam;
    }

    public float totalXP;
    public int toNextLevel = 100;
    public float XPMultiplier = 2f;

    public int currLevel = 1;

    public GameObject pickupTextPrefab;
    private PickupText newPickupText;

    private SpawnerManager spawnerManager;

    

    private Motorbike motorbike;


    private CashText cashText;

    public float slowSpeed;
    private float lowJump;
    public float runSpeedBase;
    private float jumpForceBase;

    private List<Crow> bloodbirds = new List<Crow>();

    private AchievementManager achievementManager;

    private AudioSource aSource;

    private AudioManager audioManager;

    public List<AudioClip> levelUpSounds = new List<AudioClip>();

    void Update() {
        Regen();
        painTimer -= Time.deltaTime;        
    }

    private void PlayPainSound() {
        if (painTimer <= 0) {
            int soundIndex = UnityEngine.Random.Range(0, painSounds.Count);
            aSource.clip = painSounds[soundIndex];
          //  aSource.pitch = UnityEngine.Random.Range(0.8f, 1);
            aSource.volume = audioManager.SFXVolume;
            aSource.Play();
            painTimer = UnityEngine.Random.Range(painTimerMin, painTimerMax);
        }
    }

    private void Regen() {
        if (regenTimer <= 0 && regenAmount > 0) {
            if(health < healthMax) {
                health = health + regenAmount > healthMax ? healthMax : health + regenAmount;
                regenTimer = 1f;
                newPickupText = ((GameObject)Instantiate(pickupTextPrefab, transform.position, Quaternion.identity)).GetComponent<PickupText>();

                newPickupText.setProperties("+" + (int)regenAmount, Color.green);
            }
           
        }  else {
            regenTimer -= Time.deltaTime;
        }
    }
   

    public void AddAmmo(string ammoType, int numBullets) {
        switch (ammoType) {
            case "9mm":
                nineMMRounds += numBullets;
                break;
            case ".22":
                twentyTwoRounds += numBullets;
                break;
            case "12 Ga.":
                twelveGaRounds += numBullets;
                break;
            case ".50":
                fiftyCalRounds += numBullets;
                break;
            case "7.62":
                sevenSixTwoRounds += numBullets;
                break;
            case "5.56":
                fiveFiveSixRounds += numBullets;
                break;
            case "5mm":
                fiveMMRounds += numBullets;
                break;
            case "10mm":
                tenMMRounds += numBullets;
                break;
        }
    }

    private void PlayLevelUpSound() {
        int soundIndex = UnityEngine.Random.Range(0, levelUpSounds.Count);
        audioManager.PlayClip(levelUpSounds[soundIndex]);
    }

    public int twentyTwoRounds;
    public int nineMMRounds;
    public int twelveGaRounds;
    public int fiftyCalRounds;
    public int sevenSixTwoRounds;
    public int fiveFiveSixRounds;
    public int fiveMMRounds;
    public int tenMMRounds;

    private bool isOverUI = false;

    public bool GetIsOverUI() {
        return isOverUI;
    }

    public void SetIsOverUI(bool overUI) {
        isOverUI = overUI;
    }


    public void UseAmmo(string ammoType) {
        switch (ammoType) {
            case "9mm":
                nineMMRounds--;
                break;
            case ".22":
                twentyTwoRounds--;
                break;
            case "12 Ga.":
                twelveGaRounds--;
                break;
            case ".50":
                fiftyCalRounds--;
                break;
            case "7.62":
                sevenSixTwoRounds--;
                break;
            case "5.56":
                fiveFiveSixRounds--;
                break;
            case "5mm":
                fiveMMRounds--;
                break;
            case "10mm":
                tenMMRounds--;
                break;
        }

    }


    void Start() {
        audioManager = GameObject.FindObjectOfType<AudioManager>();
        spawnerManager = GameObject.FindObjectOfType<SpawnerManager>();

        achievementManager = GameObject.FindObjectOfType<AchievementManager>();

        playerController = GetComponent<PlayerController>();

        slowSpeed = runSpeed / 2;
        lowJump = playerController.jumpForce / 2;
        runSpeedBase = runSpeed;
        jumpForceBase = playerController.jumpForce;
         
        
        motorbike = GameObject.FindObjectOfType<Motorbike>();
        cashText = GameObject.FindObjectOfType<CashText>();
      
        health = healthMax;

        aSource = GetComponent<AudioSource>();
    }

    public void AddBloodBird(Crow bloodbird) {
        if (!bloodbirds.Contains(bloodbird)) {
            bloodbirds.Add(bloodbird);
        }
        runSpeed = slowSpeed;
        playerController.jumpForce = lowJump;    
    }

    public void RemoveBloodBird(Crow bloodbird) {
        bloodbirds.Remove(bloodbird);
        if(bloodbirds.Count <= 0) {
            runSpeed = runSpeedBase;
            playerController.jumpForce = jumpForceBase;
        }
    }

    public void TakeDamage(float damage) {
        health -= CalcReceivedDamage(damage);
        achievementManager.TakeDamage(CalcReceivedDamage(damage));
        newPickupText = ((GameObject)Instantiate(pickupTextPrefab, transform.position, Quaternion.identity)).GetComponent<PickupText>();
        newPickupText.setProperties("-" + (int)CalcReceivedDamage(damage), Color.red);
        motorbike.StopRepairing();
        spawnerManager.IncreaseDamageTakenThisWave(CalcReceivedDamage(damage));
        PlayPainSound();
        if (health <= 0) {
            Die();
        }

    }



    public float GetHealthRemainingAsPercent() {
        return (health / healthMax) * 100;
    }

    public float GetCurrDamage() {
        return playerAttackDamage * damageModifier;
    }

    public float CalcReceivedDamage(float dam) {
        return dam * damageResistance;
    }

    public int GetHealth() {
        return (int)health;
    }

    private void Die() {
        motorbike.StopRepairing();
        motorbike.DisableRepair();

        //TODO: Replace with GameManager.Lose();
        // SceneManager.LoadScene(0);
        restart.SetActive(true);
        GameObject.Find("RestartMessage").GetComponent<Text>().text = "You are dead.";
        Explode();

        Destroy(gameObject);
    }

    public void AddCash(float value) {
        achievementManager.IncreaseEarnedCash(GetMoneyTotalInt(value));
        cash += GetMoneyTotalInt(value);
    }

    public void SubtractCash(int value) {
        cash -= value;
    }

    //Returns the amount of money input multiplied the moneyMultiplier
    public int GetMoneyTotalInt(float money) {
        return Mathf.CeilToInt(money * moneyMultiplier);
    }

    public int GetRepairAmount() {
        return Mathf.CeilToInt(repair * repairModifier);
    }

    public float GetRunSpeed() {
        return runSpeed;
    }

    public int GetMaxJumps() {
        return maxJumps;
    }

    public void GainXP(int XP) {
        totalXP += (XP * XPMultiplier);

        if (totalXP >= toNextLevel && currLevel < maxLevel) {
            lastLevelXP = toNextLevel;
            currLevel++;
            skillPoints++;
            toNextLevel = Mathf.Clamp((int)(currLevel * 50 * Mathf.Log10(currLevel)), 0, 5000);
            totalXP = 0;
            newPickupText = ((GameObject)Instantiate(pickupTextPrefab, transform.position, 
                Quaternion.identity)).GetComponent<PickupText>();

            newPickupText.gameObject.transform.parent = this.gameObject.transform;

            newPickupText.setProperties("Level Up!", Color.blue);
            PlayLevelUpSound();
            newPickupText.transform.localScale *= 1.25f;
        }
      
    }

    public bool IsMaxLevel() {
        return currLevel >= maxLevel;
    }

    public int GetTotalXPGainedInt(int XP) {
        return Mathf.CeilToInt(XP * XPMultiplier);
    }

    public int GetAmmoPickupCount(int amountIn) {
        return Mathf.CeilToInt(amountIn * ammoMultiplier);
    }

    internal int GetRoundsOwned(string ammoType) {
        switch (ammoType) {
            case "9mm":
                return nineMMRounds;
            case ".22":
                return twentyTwoRounds;
            case "12 Ga.":
                return twelveGaRounds;
            case ".50":
                return fiftyCalRounds;
            case "7.62":
                return sevenSixTwoRounds;
            case "5.56":
                return fiveFiveSixRounds;
            case "5mm":
                return fiveMMRounds;
            case "10mm":
                return tenMMRounds;
            default:
                return 0;
        }
    }

    public void GainHealth(float amount) {
        health += Mathf.CeilToInt((healthMax * amount) * healthGainMultiplier);
        if (health > healthMax) {
            health = healthMax;
        }      
    }

   

    public int GetHealthGainTotal(float amount) {
        return Mathf.CeilToInt((healthMax * amount) * healthGainMultiplier);
    }

    private void Explode() {
       
            Instantiate(legPrefab, leftLegPosition.transform.position, Quaternion.identity);
            Instantiate(legPrefab, rightLegPosition.transform.position, Quaternion.identity);
            Instantiate(torsoPrefab, torsoPosition.transform.position, Quaternion.identity);
            Instantiate(armPrefab, leftArmPosition.transform.position, Quaternion.identity);
            Instantiate(armPrefab, rightArmPosition.transform.position, Quaternion.identity);
            Instantiate(headPrefab, headPosition.transform.position, Quaternion.identity);

        
    }
}


