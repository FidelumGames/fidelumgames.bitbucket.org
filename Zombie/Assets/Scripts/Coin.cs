﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

    public float value;
    public float diff;

    public float maxSpinSpeed;
    public float minSpinSpeed;
    
    public float moveSpeed = 10f;
    public float spinSpeed;
    private Vector3 rotateTemp;

    public GameObject pickupText;
    public PickupText newPickup;
    private Stats stats;

    public GameObject coinSoundPrefab;

    private bool shouldDie = false;
    

    // Use this for initialization
    void Start() {
        spinSpeed = Random.Range(minSpinSpeed, maxSpinSpeed);
        rotateTemp = transform.eulerAngles;
        stats = GameObject.FindObjectOfType<Stats>();
    }

    // Update is called once per frame
    void Update() {
        if (shouldDie) {
            Destroy(gameObject);
        }
        rotateTemp.y += Time.deltaTime * spinSpeed;
        transform.eulerAngles = rotateTemp;
        AttractToMagnet();
        
    }

    void AttractToMagnet() {

        if (stats) {
            diff = Vector2.Distance(transform.position, stats.transform.position);


            if (diff <= stats.magnetStrength) {
                PickupStabilizer stabilizer = GetComponent<PickupStabilizer>();
                if (stabilizer) {
                    Destroy(stabilizer);
                }
                if (stats) {
                    transform.position = Vector2.MoveTowards(transform.position, stats.transform.position,
                        moveSpeed * Time.deltaTime);
                }

            }

            if (diff <= .25f) {
                shouldDie = true;
                newPickup = ((GameObject)Instantiate(pickupText, transform.position, Quaternion.identity)).GetComponent<PickupText>();
                Instantiate(coinSoundPrefab);
                // newPickup.transform.position = transform.position;
                newPickup.setProperties("$" + stats.GetMoneyTotalInt(value), Color.green);
                stats.AddCash(value);

            }
        }
       
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "Player") {
            shouldDie = true;
            Instantiate(coinSoundPrefab);
            newPickup = ((GameObject)Instantiate(pickupText, transform.position, Quaternion.identity)).GetComponent<PickupText>();
           // newPickup.transform.position = transform.position;
            newPickup.setProperties("$" + stats.GetMoneyTotalInt(value),  Color.green);
            stats.AddCash(value);
            
        }
    }
}
