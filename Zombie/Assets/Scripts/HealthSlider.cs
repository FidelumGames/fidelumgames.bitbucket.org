﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthSlider : MonoBehaviour {

    private Text text;
    private Slider slider;
    public Stats stats;

    void Start() {
        text = GetComponentInChildren<Text>();
        slider = GetComponent<Slider>();
        stats = GameObject.FindObjectOfType<Stats>();
      //  print("1 " + Time.time);
    }

    public void UpdateUI() {
        stats = GameObject.FindObjectOfType<Stats>();
        text = GetComponentInChildren<Text>();
        slider = GetComponent<Slider>();
        // print("2 " + Time.time);
        if (stats) {
            text.text = stats.GetHealth() + "/" + stats.healthMax + " (" +
                 (int)((float)stats.GetHealth() / (float)stats.healthMax * 100) + "%)";
            slider.value = ((float)stats.GetHealth() / (float)stats.healthMax);
        }
    }
    
}
