﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {

    public int healthMin, healthMax;
    public GameObject pickupSound;
    public GameObject pickupTextPrefab;
    private PickupText newPickupText;

    public float spinSpeed = 1;
    private Vector3 rotateTemp;
    public float maxSpinSpeed;
    public float minSpinSpeed;

    void Start() {
        spinSpeed = Random.Range(minSpinSpeed, maxSpinSpeed);
    }

    void OnTriggerEnter2D(Collider2D col) {
        Stats stats = col.GetComponent<Stats>();
        if (stats) {

            float amount = Random.Range((float)healthMin, (float)healthMax) / 100;
            stats.GainHealth(amount);
            newPickupText = ((GameObject)Instantiate(pickupTextPrefab, transform.position, Quaternion.identity)).GetComponent<PickupText>();
           
            newPickupText.setProperties("+" + (int)stats.GetHealthGainTotal(amount), Color.green);
            Instantiate(pickupSound);
            Destroy(gameObject);
        }
    }

    void Update() {
        Spin();
    }

    void Spin() {
        rotateTemp.y += Time.deltaTime * spinSpeed;
        transform.eulerAngles = rotateTemp;
    }




}
