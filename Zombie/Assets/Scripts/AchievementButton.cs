﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementButton : MonoBehaviour {

    public Sprite icon;
    public GameObject achievementPrefab;

    public enum AchievementType {
        WeaponUpgrade, None
    }

    public AchievementType achievementType = AchievementType.None;

    private AchievementManager achievementManager;
    private bool isUnlocked = false;
    public int IsUnlocked
    {
        get { return isUnlocked ? 1 : 0; }
        set {
            isUnlocked = value == 1 ? true : false;
            if (isUnlocked) {
                LoadUnlock();
            }
        }
    }


    public string descriptionText = "";
    private string progressText = "";
    public string rewardText;

    private GameObject infoPanel;
    private Text infoPanelTitle;
    private Text infoPanelDescription;
    private Text infoPanelProgress;
    private Text infoPanelReward;

    private UIManager uiManager;

    public void Unlock() {
        foreach (Transform child in transform) {
            child.GetComponent<Image>().sprite = icon;
        }
        isUnlocked = true;
        ShowAchievement();
    }

    //Unlock an achievement without showing it.
    public void LoadUnlock() {
        foreach (Transform child in transform) {
            child.GetComponent<Image>().sprite = icon;
        }
        isUnlocked = true;
    }

    private void ShowAchievement() {
        GameObject achievementBox = (GameObject)Instantiate(achievementPrefab, new Vector3(700, 507, -750),
            Quaternion.identity, GameObject.Find("Canvas").transform);

        achievementBox.name = this.name + "box";

        GameObject.Find(this.name + "box/Icon").GetComponent<Image>().sprite = icon;
        GameObject.Find(this.name + "box/TitleText").GetComponent<Text>().text = name;
        GameObject.Find(this.name + "box/DescriptionText").GetComponent<Text>().text = "Achievement  Unlocked:\n" + descriptionText;
        GameObject.Find(this.name + "box/RewardText").GetComponent<Text>().text = "Reward:" + rewardText;

    }

    public void ShowInfoPanel() {
        if (!infoPanel || !infoPanelDescription || !infoPanelProgress || !infoPanelTitle) {
            Start();
        }
        infoPanelTitle.text = isUnlocked ? name : "???????";
        infoPanelDescription.text = descriptionText;

        infoPanelProgress.text = progressText;
        infoPanelReward.text = "Reward: " + rewardText;
        float xPos = Mathf.Clamp(Input.mousePosition.x, 0, 562);
        float yPos = Input.mousePosition.y + 15;
        infoPanel.GetComponent<RectTransform>().position = new Vector2(xPos, yPos);
        infoPanel.SetActive(true);

    }



    public void HideInfoPanel() {
        if (!infoPanel || !infoPanelDescription || !infoPanelProgress || !infoPanelTitle) {
            Start();
        }
        infoPanel.SetActive(false);
    }

    void Start() {
        uiManager = uiManager ? uiManager : GameObject.FindObjectOfType<UIManager>();
        achievementManager = achievementManager ? achievementManager : GameObject.FindObjectOfType<AchievementManager>();
        infoPanel = uiManager.achievementInfoPanel;
        infoPanelTitle = uiManager.infoPanelTitle;
        infoPanelDescription = uiManager.infoPanelDescription;
        infoPanelProgress = uiManager.infoPanelProgress;
        infoPanelReward = uiManager.infoPanelReward;
    }

    void Update() {
        progressText = achievementManager.GetProgressText(name);
    }

}
