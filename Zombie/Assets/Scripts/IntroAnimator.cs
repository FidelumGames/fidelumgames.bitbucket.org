﻿using UnityEngine;
using System.Collections;

public class IntroAnimator : MonoBehaviour {

    private Transform frontWheel, rearWheel, frame;
    ParticleSystem exhaust;

    public float wheelRotateSpeedBase;
    public Vector2 frameRumbleBase;
    public float frameRumbleUpdateTime = .1f;
    private float frameRumbleUpdateTimeElapsed;
    public bool bikeShouldAnimate = true;

    public SpriteRenderer background;

    public float paralaxBaseSpeed;

	void Start () {
        frontWheel = GameObject.Find("Front Wheel").transform;
        rearWheel = GameObject.Find("Rear Wheel").transform;
        frame = GameObject.Find("Frame").transform;
        exhaust = GameObject.Find("Exhaust").GetComponent<ParticleSystem>();
    }
	
	void Update () {
        if (bikeShouldAnimate) {
            Animate();
        }
	}

    private void Animate() {
        frontWheel.Rotate(Vector3.back * Time.deltaTime * wheelRotateSpeedBase);
        rearWheel.Rotate(Vector3.back * Time.deltaTime * wheelRotateSpeedBase);

        if (frameRumbleUpdateTimeElapsed > frameRumbleUpdateTime) {
            frame.localPosition = new Vector3(Random.Range(-frameRumbleBase.x, frameRumbleBase.x),
                                            Random.Range(-frameRumbleBase.y, frameRumbleBase.y), 0);
            frameRumbleUpdateTimeElapsed = 0;
        } else {
            frameRumbleUpdateTimeElapsed += Time.deltaTime;
        }       
    }

    

    
}
