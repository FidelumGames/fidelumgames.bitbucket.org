﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AmmoPickup : MonoBehaviour {



    public GameObject ammoSound;
    public int maxBullets = 100;
    public int minBullets = 10;

    private int numBullets = 0;
    private int index; //Which item in the weapons List to spawn ammo for

    public GameObject pickupTextPrefab;
    private PickupText newPickupText;
    private List<WeaponController> weapons;
    private WeaponUI weaponUI;
    private Stats stats;
    private SpriteRenderer spriteRenderer;

    public float spinSpeed = 1;
    private Vector3 rotateTemp;
    public float maxSpinSpeed;
    public float minSpinSpeed;

    void Start() {
        spinSpeed = Random.Range(minSpinSpeed, maxSpinSpeed);

        weaponUI = GameObject.FindObjectOfType<WeaponUI>();
        stats = GameObject.FindObjectOfType<Stats>();
        weapons = GameObject.FindObjectOfType<WeaponChanger>().weapons;
        spriteRenderer = GetComponent<SpriteRenderer>();
        int sentinel = 0;
        do {
            index = Random.Range(0, weapons.Count);
            sentinel++;
        } while (!weapons[index].isUnlocked && sentinel <= 1000);

        spriteRenderer.sprite = weapons[index].ammoIcon;

        numBullets = stats.GetAmmoPickupCount(Random.Range(minBullets, maxBullets));
    }

    void Update() {
        Spin();
    }

    void OnTriggerEnter2D(Collider2D col) {

        if (col.GetComponent<Stats>()) {
            Instantiate(ammoSound);
            stats.AddAmmo(weapons[index].caliber, numBullets);
            weaponUI.UpdateUI(weapons[index]);
            newPickupText = ((GameObject)Instantiate(pickupTextPrefab, transform.position,
                Quaternion.identity)).GetComponent<PickupText>();
            newPickupText.setProperties(weapons[index].ToString() + "  x" + numBullets, new Color(141, 96, 0));
            Destroy(gameObject);
        }
    }

    void Spin() {
        rotateTemp.y += Time.deltaTime * spinSpeed;
        transform.eulerAngles = rotateTemp;
    }
}
