﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureParallaxer : MonoBehaviour {

    public List<Renderer> parallaxLayers = new List<Renderer>();
    public float frontLayerSpeed;
    public float[] layerSpeeds;
    

	// Use this for initialization
	void Start () {
        layerSpeeds = new float[parallaxLayers.Count];
        SetLayerSpeeds();
	}

    private void SetLayerSpeeds() {
        float percentage = frontLayerSpeed / layerSpeeds.Length;

        for(int i = 0; i < layerSpeeds.Length; i++) {
            layerSpeeds[i] = frontLayerSpeed - percentage * i;
        }
    }
	
	// Update is called once per frame
	void Update () {
        SetLayerSpeeds();
      

        for (int i = 0; i < parallaxLayers.Count; i++) {
            Vector2 tiling = parallaxLayers[i].material.mainTextureOffset;
            tiling.x += layerSpeeds[i] * Time.deltaTime * parallaxLayers[i].material.mainTextureScale.x;
            parallaxLayers[i].material.mainTextureOffset = tiling;
        }

    }
}
