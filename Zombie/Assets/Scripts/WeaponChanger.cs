﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WeaponChanger : MonoBehaviour {


    public Stats stats;

    public GameObject reloadFill;
    public SpriteRenderer reloadOutline;
    public List<WeaponController> weapons;

    public int currWeapon = 0;
    public WeaponUI weaponUI;

    void Start() {
        stats = GameObject.FindObjectOfType<Stats>();

      //  weaponUI = GameObject.FindObjectOfType<WeaponUI>();

        //Disable all weapons
        foreach (WeaponController weapon in weapons) {

            weapon.transform.gameObject.SetActive(false);
        }

        //Enable only the active weapon
        weapons[currWeapon].transform.gameObject.SetActive(true);
        stats.SetDamage((int)weapons[currWeapon].damage);

        //Initialize the reload UI
        reloadFill = GameObject.Find("ReloadFill");
        reloadOutline = GameObject.Find("ReloadOutline").GetComponent<SpriteRenderer>();
    }

    public WeaponController GetCurrentWeapon() {
        return weapons[currWeapon];
    }

    public void UpdateStats() {
        stats.SetDamage((int)weapons[currWeapon].damage);
    }


    void Update() {
        if (Input.GetAxisRaw("Mouse ScrollWheel") > 0 || Input.GetKeyDown(KeyCode.Keypad0) 
            || Input.GetKeyDown(KeyCode.Alpha3)) {
            ResetReload();
            SwitchWeapon("Up");

            //Equip Previous weapon
        } else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0 || Input.GetKeyDown(KeyCode.RightControl)
            || Input.GetKeyDown(KeyCode.Alpha1)) {
            ResetReload();
            SwitchWeapon("Down");
        }
    }

    

    void SwitchWeapon(string direction) {
        if (direction == "Up") {
            currWeapon++;
            currWeapon = currWeapon >= weapons.Count ? 0 : currWeapon;
        } else {
            currWeapon--;
            currWeapon = currWeapon <= -1 ? weapons.Count - 1 : currWeapon;
        }

        //Equip weapon if it's unlocked, otherwise, move to next weapon.
        if (weapons[currWeapon].isUnlocked) {
            EnableWeapon(weapons[currWeapon]);
            weaponUI.UpdateUI(weapons[currWeapon]);
            stats.SetDamage((int)weapons[currWeapon].damage);
        } else {
            SwitchWeapon(direction);
        }
    }

    //Disable all weapons except the one passed to the method
    void EnableWeapon(WeaponController weapon) {
        foreach (GameObject otherWeapon in GameObject.FindGameObjectsWithTag("Weapon")) {
            if (otherWeapon != weapon.transform.gameObject) {
                otherWeapon.SetActive(false);
            }
        }
        weapon.transform.gameObject.SetActive(true);
    }

    public void EnableWeapon(string weaponName) {
        foreach (GameObject otherWeapon in GameObject.FindGameObjectsWithTag("Weapon")) {
            if (otherWeapon.name != weaponName) {
                otherWeapon.SetActive(false);
            } else {
                otherWeapon.SetActive(true);
            }
        }

        foreach(WeaponController weapon in weapons) {
            if (weapon.name == weaponName){
                weaponUI.UpdateUI(weapon);
                stats.SetDamage((int)weapon.damage);
                currWeapon = weapons.IndexOf(weapon);
            }
        }
        
    }

    //Reset the reload timer for the current weapon
    void ResetReload() {
        weapons[currWeapon].elapsedReloadTime = 0;
        reloadFill.transform.localScale = new Vector3(0, 1.674579f, 1.674579f);
        reloadOutline.enabled = false;
    }
}
