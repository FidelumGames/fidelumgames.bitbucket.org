﻿using UnityEngine;
using System.Collections;

public class ShooterTitle : MonoBehaviour {

    private GameObject startButton, clearDataButton;

	void Start () {
        startButton = GameObject.Find("StartButton");
        clearDataButton = GameObject.Find("ClearButton");
        startButton.SetActive(false);
        clearDataButton.SetActive(false);
	}

    public void ShowStartButton() {
        startButton.SetActive(true);
        clearDataButton.SetActive(true);
        GameObject.Find("AudioManager").GetComponent<AudioSource>().enabled = true;
    }
	
	
	
}
