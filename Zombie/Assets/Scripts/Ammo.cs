﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ammo : MonoBehaviour {

    public int cost = 10;
    public int numBullets = 10;
    public string caliber = ".22";
    
    //A list of descriptions created so that descriptions can be easily typed inside the script.
    private List<string> descriptions = new List<string>();

    //Instance's description
    public int descriptionIndex = 0;

    private Stats stats;
    private UIManager uiManager;
    private Shop shop;

    void Start() {
        stats = GameObject.FindObjectOfType<Stats>();
        uiManager = GameObject.FindObjectOfType<UIManager>();
        shop = GameObject.FindObjectOfType<Shop>();  
        AddDescriptions();        
    }

    private void AddDescriptions() {
        descriptions.Add("Description:\nStandard  .22  rimfire rounds.\nThey  tend  to  bounce  around  " +
           "inside  the  skull:) \n\nUsed by:\n.22  revolver");
        descriptions.Add("Description:\nHollow point 9mm rounds.\nSmall  hole  in,  big  hole  out." +
        "\n\nUsed  by:\n9mm  Beretta,  Uzi");
        descriptions.Add("Description:\n12  gauge  buckshot.\nA  nice  spread  for  turning" + 
            "  lots  of  things  to  jelly.\n\nUsed  by:\nPump-Action  Shotgun,  Combat Shotgun,  AA-12");
        descriptions.Add("Description:\n10mm  ammunition.\nPacks a real punch." +
            "\n\nUsed  by:\nMP5");
        descriptions.Add("Description:\n7.62  ammunition.\nSold by warlords around the world." +
            "Before it ended, that is.\n\nUsed  by:\nAK47");
        descriptions.Add("Description:\n.50 cal  ammunition.\nBig ammo + Big rifle = red jello." +
            "\n\nUsed  by:\nSniper Rifle");
        descriptions.Add("Description:\n5.56  ammunition.\nShort.  And to the point." +
            "\n\nUsed  by:\nM4A1");
        descriptions.Add("Description:\n5mm  ammunition.\nSmall. Like a hornet. If hornets" +
            "were metal and put big holes in you.\n\nUsed  by:\nMinigun");
    }

    public void BuyMe() {
        shop.BuyAmmo(this);
        ShowMyInfo();
    }

    public void ShowMyInfo() {
        uiManager.ShowItemInfoPanel(cost, descriptions[descriptionIndex]);       
    }

}


