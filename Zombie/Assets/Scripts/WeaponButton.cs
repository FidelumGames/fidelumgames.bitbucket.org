﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WeaponButton : MonoBehaviour {
    
    //A list of descriptions created so that descriptions can be easily typed inside the script.
    private List<string> descriptions = new List<string>();

    //Instance's description
    public int descriptionIndex = 0;
    public int cost = 100;

   
    private UIManager uiManager;
    private Shop shop;
    
    private List<WeaponController> weapons;
    public WeaponController myWeapon;
    public string myDescription;


    void Start() {
        uiManager = GameObject.FindObjectOfType<UIManager>();
        shop = GameObject.FindObjectOfType<Shop>();
        weapons = GameObject.FindObjectOfType<WeaponChanger>().weapons;     
        SetMyWeapon();     
    }    

    private void SetMyWeapon() {
        foreach(WeaponController weapon in weapons) {
            if (weapon.transform.name == transform.name) {
                myWeapon = weapon;
                return;
            }            
        }
     //   Debug.LogError(transform.name + " button has no weapon assigned to it!");
    }

    

    public void BuyMe() {
        shop.BuyWeapon(this);
        ShowMyInfo();
    }

    public void MakeMeActiveButton() {
        uiManager.SetActiveWeaponButton(this);
    }

    public void ShowMyInfo() {

        string rateOfFire = myWeapon.transform.name == "9mm" ? "10" : string.Format("{0:F1}", 1.0f / (float)myWeapon.delayBase);
        string accuracy = string.Format("{0:F0}", ((5f - myWeapon.accuracyModMax) / 5f) * 100); 

        myDescription = "Damage: " + myWeapon.damage +
                                "\nRate Of Fire: " + rateOfFire + " rounds/s" +
                                "\nReload Time: " + string.Format("{0:F1}", myWeapon.reloadTime) + " seconds" +
                                "\nCapacity: " + myWeapon.capacity + 
                                "\nAccuracy: %" + accuracy + 
                                "\nNumber Of Projectiles: " + myWeapon.numProjectiles;
        uiManager.ShowWeaponInfoPanel(myWeapon.name,0, myDescription);       
    }

}


