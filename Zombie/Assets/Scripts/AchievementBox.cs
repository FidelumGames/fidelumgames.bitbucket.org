﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AchievementBox : MonoBehaviour {

    public float fadeRate = 1f;
    public float fadeTimer = 2f;

    private Color color;

	
	void Start () {
        color = GetComponentInChildren<Image>().color;
        GetComponent<AudioSource>().volume = GameObject.FindObjectOfType<AudioManager>().SFXVolume;
	}
	
	// Update is called once per frame
	void Update () {
	    if(fadeTimer > 0) {
            fadeTimer -= Time.deltaTime;
        } else {
            color.a -= Time.deltaTime;
            GetComponent<Image>().color = color;
            foreach(Transform child in transform) {
                if (child.GetComponent<Image>()) {
                    child.GetComponent<Image>().color = color;                    
                } else {
                    if (child.GetComponent<Text>()) {
                        child.GetComponent<Text>().color = color;
                    }
                }
            }

            if(color.a <= 0) {
                Destroy(gameObject);
            }
        }
	}
}
