﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Shop : MonoBehaviour {
    private AchievementManager achievementManager;

    private Stats stats;
    private UIManager uiManager;
    private AudioManager audioManager;
    public Sprite purchasedSprite;

    void Start() {
        achievementManager = GameObject.FindObjectOfType<AchievementManager>();
        stats = GameObject.FindObjectOfType<Stats>();
        uiManager = GameObject.FindObjectOfType<UIManager>();
        audioManager = GameObject.FindObjectOfType<AudioManager>();
    }

    public void BuyAmmo(Ammo ammo) {

        //Buy Ammo
        if (stats.cash >= ammo.cost) {
            stats.SubtractCash(ammo.cost);
            stats.AddAmmo(ammo.caliber, ammo.numBullets);

            uiManager.ShowUIText(ammo.caliber + "  x" + ammo.numBullets, UIManager.UIColor.Yellow, "mouse");
            uiManager.ShowUIText("-$" + ammo.cost, UIManager.UIColor.Green, "CashText");

            audioManager.PlayBuySound();

            //Not enough money         
        } else {
            uiManager.ShowUIText("Not enough cash!", UIManager.UIColor.Red, "mouse");
            audioManager.PlayErrorSound();
        }
    }

    public void BuyHealth(Health healthIn) {

        //Buy Health
        if (stats.cash >= healthIn.cost && stats.health < stats.healthMax) {
            stats.SubtractCash(healthIn.cost);
            float healthTemp = stats.health + (stats.healthMax * healthIn.amount);
            stats.health = healthTemp > stats.healthMax ? stats.healthMax : healthTemp;

            uiManager.ShowUIText("+" + (int)(healthIn.amount * stats.healthMax) + " HP",
                UIManager.UIColor.Green, "mouse");

            uiManager.ShowUIText("-$" + (int)healthIn.cost, UIManager.UIColor.Green, "CashText");

            uiManager.UpdateUI();
            audioManager.PlayBuySound();

            //Not enough money
        } else if (stats.cash < healthIn.cost) {
            uiManager.ShowUIText("Not enough cash!", UIManager.UIColor.Red, "mouse");
            audioManager.PlayErrorSound();

            //Already at full health
        } else {
            uiManager.ShowUIText("Already at full health!", UIManager.UIColor.Red, "mouse");
            audioManager.PlayErrorSound();
        }
    }

    public void BuyWeapon(WeaponButton weapon) {

        //Buy Ammo
        if (stats.cash >= weapon.cost) {
            achievementManager.BuyWeapon();
            
            stats.SubtractCash(weapon.cost);
            weapon.myWeapon.isUnlocked = true;

            uiManager.ShowUIText(weapon.name + " unlocked!", UIManager.UIColor.Green, "mouse");
            uiManager.ShowUIText("-$" + weapon.cost, UIManager.UIColor.Green, "CashText");

            audioManager.PlayBuySound();

            //Not enough money         
        } else {
            uiManager.ShowUIText("Not enough cash!", UIManager.UIColor.Red, "mouse");
            audioManager.PlayErrorSound();
        }
    }

    public void BuyUpgrade(UpgradeButton upgrade) {


        int cost = upgrade.costs[upgrade.currLevel];
        float value = upgrade.levelValues[upgrade.currLevel];
        WeaponController weapon = upgrade.myWeapon;

        if (stats.cash >= cost) {
            achievementManager.UpgradeWeapon(upgrade.myWeapon);
            GameObject.Find(upgrade.gameObject.name + "/UpgradeLvl" + upgrade.currLevel).GetComponent<Image>().sprite = purchasedSprite;
            stats.SubtractCash(upgrade.costs[upgrade.currLevel]);

            switch (upgrade.upgrade) {
                case ("Damage"):
                    weapon.damage = value;
                    break;
                case ("Accuracy"):
                    weapon.accuracyModMax = value;
                    weapon.accuracyModMin = value * -1f;
                    break;
                case ("Reload Time"):
                    weapon.reloadTime = value;
                    break;
                case ("Capacity"):
                    weapon.capacity = (int)value;
                    break;
                case ("Fire Rate"):
                    weapon.delayBase = value;
                    break;
                case ("Projectiles"):
                    weapon.numProjectiles = (int)value;
                    break;
            }

            upgrade.myWeapon.isUnlocked = true;

            uiManager.ShowUIText(upgrade.myWeapon + " " + upgrade.upgrade + " upgraded!", UIManager.UIColor.Green, "mouse");
            uiManager.ShowUIText("-$" + upgrade.costs[upgrade.currLevel], UIManager.UIColor.Green, "CashText");
            upgrade.currLevel++;

            uiManager.ShowUpgradeInfoPanel();

            audioManager.PlayBuySound();

            //Not enough money         
        } else {
            uiManager.ShowUIText("Not Enough Money!", UIManager.UIColor.Red, "mouse");
            audioManager.PlayErrorSound();
        }
    }

    public void UpgradeAbility(AbilityButton upgrade) {

        int currentLevel = upgrade.currLevel;
        float[] values = upgrade.levelValues;

        if (currentLevel >= values.Length) {
            uiManager.ShowUIText("Ability is maxed!", UIManager.UIColor.Red, "mouse");
            audioManager.PlayErrorSound();
        } else if (stats.skillPoints >= upgrade.currLevel + 1) {
            achievementManager.UpgradeAbility();
            stats.skillPoints -= upgrade.currLevel + 1;
            switch (upgrade.gameObject.name) {
                case "Multi-Jump":
                    stats.maxJumps = (int)values[currentLevel];
                    GameObject.FindObjectOfType<PlayerController>().jumpsLeft = stats.maxJumps;
                    break;
                case "Grease Monkey":
                    stats.repairModifier++;
                    break;
                case "Fast Feet":
                    stats.runSpeedBase = values[currentLevel];                    
                    stats.runSpeed = stats.runSpeed == stats.slowSpeed ? stats.runSpeedBase / 2 :
                        stats.runSpeedBase;
                    stats.slowSpeed = stats.runSpeedBase / 2;
                    break;
                case "Fast Learner":
                    stats.XPMultiplier = values[currentLevel];
                    break;
                case "Iron Skin":
                    stats.damageResistance -= values[currentLevel];
                    break;
                case "Lucky":
                    stats.moneyMultiplier += values[currentLevel];
                    break;
                case "Juggernaut":
                    stats.healthMax += (int)values[currentLevel];
                    break;
                case "Attractive":
                    stats.magnetStrength = values[currentLevel];
                    break;
                case "Hydra Man":
                    stats.regenAmount = (int)values[currentLevel];
                    break;
                case "Fast Fingers":
                    stats.reloadModifier = values[currentLevel];
                    break;
                case "Steady Hands":
                    stats.accuracyModifier += values[currentLevel];
                    break;
            }

            GameObject.Find(upgrade.gameObject.name + "UpgradeIcons/UpgradeLvl" + upgrade.currLevel).
                GetComponent<Image>().sprite = purchasedSprite;

            uiManager.ShowUIText(upgrade.name + " upgraded!", UIManager.UIColor.Green, "mouse");
            uiManager.ShowUIText("-" + (currentLevel + 1), UIManager.UIColor.Blue, "AbilityPointsLocation");
            upgrade.currLevel++;

            upgrade.ShowMyInfo();

            audioManager.PlayBuySound();

        } else {
            uiManager.ShowUIText("Not enough skill points!", UIManager.UIColor.Red, "mouse");
            audioManager.PlayErrorSound();
        }
    }
}
