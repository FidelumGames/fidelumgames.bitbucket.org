﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartMenu : MonoBehaviour {

    private GameObject canvas, managers, player, spawners, zombieTitle;

    public Text text;
    public Button clearData;

    public SpawnerManager spawnerManager;
    public Motorbike motorBike;
    private SpriteRenderer blackout;

    void Start() {
        canvas = GameObject.Find("Canvas");
        managers = GameObject.Find("Managers");
        player = GameObject.Find("Player");
        spawners = GameObject.Find("Spawners");
        blackout = GameObject.Find("Blackout").GetComponent<SpriteRenderer>();
        zombieTitle = GameObject.Find("ZombieTitle");
        HideGameElements();

        if (PlayerPrefs.HasKey("Fight Another Day...")) {
            
            if (PlayerPrefs.GetInt("Fight Another Day...") == 1) {
                text.text = "New Game+";
                PlayerPrefs.SetInt("currentWave", 30);
                spawnerManager.currentWave = 30;
            } else {
                text.text = "Continue";               
            }

            clearData.interactable = true;
        } else {
            clearData.interactable = false;
        }
    }

    public void HideGameElements() {
        canvas.SetActive(false);
        managers.SetActive(false);
        player.SetActive(false);
        spawners.SetActive(false);
        GameObject.FindObjectOfType<Motorbike>().GetComponent<Motorbike>().enabled = false;
    }

    public void ShowGameElements() {
        canvas.SetActive(true);
        managers.SetActive(true);
        player.SetActive(true);
        spawners.SetActive(true);
        GameObject.FindObjectOfType<Motorbike>().GetComponent<Motorbike>().enabled = true;
        Destroy(this.gameObject);
    }

    public void playZombieAnimation() {
        zombieTitle.GetComponent<Animator>().enabled = true;
    }
}
