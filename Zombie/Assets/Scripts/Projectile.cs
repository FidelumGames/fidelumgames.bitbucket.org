﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using System.Linq;

public class Projectile : MonoBehaviour {

    public float skinbieHeadshotHeight = -.5f;
    public float slenderbieHeadshotHeight = -.5f;

    private Stats stats;
    private Rigidbody2D myRigidbody;

    public float killTime = 1f;
    public AudioClip shotSound;

    void Update() {
        if(killTime <= 0) {
            Destroy(gameObject);
        }
        killTime -= Time.deltaTime;
    }

    void Start() {
      
        stats = GameObject.FindObjectOfType<Stats>();
        myRigidbody = GetComponent<Rigidbody2D>();

      

        //TODO: MOVE THE AUDIO PLAYING INTO THE FIRE FUNCTION OF WEAPON CONTROLLER
        
    }

    
 
    void OnCollisionEnter2D(Collision2D col) {

        Enemy enemy = col.gameObject.GetComponent<Enemy>();
        Crow crow = col.gameObject.GetComponentInParent<Crow>();
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;

        GetComponent<Rigidbody2D>().isKinematic = true;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        //  GetComponent<TrailRenderer>().enabled = false;

       if (enemy && enemy.health > 0) {
            if(enemy.enemyType == Enemy.EnemyType.Skinbie) {
                if(col.contacts[0].point.y > skinbieHeadshotHeight) {
                    enemy.TakeDamage(stats.GetCurrDamage() * 2);
                } else {
                    enemy.TakeDamage(stats.GetCurrDamage());
                }
            } else if (enemy.enemyType == Enemy.EnemyType.Slenderbie) {
                if (col.contacts[0].point.y > slenderbieHeadshotHeight) {
                    enemy.TakeDamage(stats.GetCurrDamage() * 2);
                } else {
                    enemy.TakeDamage(stats.GetCurrDamage());
                }
            }
       }

        if (crow && crow.health > 0) {
            crow.TakeDamage(stats.GetCurrDamage());
        }        
    }

   // void OnCollisionStay2D(Collision2D col) {
   //     Enemy enemy = col.gameObject.GetComponent<Enemy>();
        
   //     GetComponent<MeshRenderer>().enabled = false;
    //    GetComponent<Collider2D>().enabled = false;
   //     GetComponent<Rigidbody2D>().isKinematic = true;
       
   //     if (enemy) {
  //          enemy.TakeDamage(stats.GetCurrDamage());
   //     }
   // }
}
