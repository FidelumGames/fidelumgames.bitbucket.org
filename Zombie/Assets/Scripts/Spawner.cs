﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    public GameObject enemyPrefab;
    private int spawnedEnemies;

    public float delayMax = 1f;
    public float delayMin = 0f;
    private float delay = 0f;

    void Update() {
        delay -= Time.deltaTime;
        Spawn();
    }

    void Spawn() {
        if(delay <= 0) {
            Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            delay = Random.Range(delayMin, delayMax);
            spawnedEnemies++;
        }
    }    

    void OnDrawGizmos() {
        Gizmos.DrawWireSphere(transform.position, 1f);
    }

    public int GetSpawnedEnemyCount() {
        return spawnedEnemies;
    }

    public void ResetSpawnedEnemyCount() {
        spawnedEnemies = 0;
    }

    public void SetValues(Vector2 spawnRange, bool isEnabled) {
        delayMin = spawnRange.x;
        delayMax = spawnRange.y;

        this.enabled = isEnabled;
    }
}
