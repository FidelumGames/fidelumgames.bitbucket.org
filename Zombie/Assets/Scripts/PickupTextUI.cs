﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PickupTextUI : MonoBehaviour {

    public float ascentSpeed;
    public float opacitySpeed;
    private float opacityStart = 1.5f;

    public float xRangeMin;
    public float xRangeMax;
    public float yRangeMin;
    public float yRangeMax;

    public Vector3 posTemp;

    public Text pickupText;

    public Color color;

    public string myText;

    void Start() {
        posTemp = transform.position;
        posTemp.x += Random.Range(xRangeMin, xRangeMax);
        posTemp.y += Random.Range(yRangeMin, yRangeMax);
        posTemp.z = -25;
        pickupText = GetComponent<Text>();
        pickupText.text = myText;
        pickupText.color = new Color(0, 0, 0, 0);
    }

    public void setProperties(string text, Color color) {
        myText = text;
        this.color = color;
    }

    void Update() {
        posTemp.y += ascentSpeed;
        transform.position = posTemp;
        opacityStart -= opacitySpeed;

        color.a = opacityStart;
        pickupText.color = color;
        
        if (pickupText.color.a <= 0) {
            Destroy(gameObject);
        }
    }
}
