﻿using UnityEngine;
using System.Collections;

public class Casing : MonoBehaviour {
    
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D myRigidbody;

    public float maxForce, minForce, minTorque, maxTorque, fadeRate;
    public float lifeTime = 10f;
    private bool fade = false;
    private Color color;

    private bool hasStarted = false;

    void Start() {
        myRigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        color = spriteRenderer.color;

        float torque = Random.Range(minTorque, maxTorque);

        Vector2 force = new Vector2(Random.Range(minForce, maxForce), Random.Range(minForce, maxForce));

        if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x > transform.position.x) {
            force.x *= -1;
        }
          

        myRigidbody.AddTorque(torque);
        myRigidbody.AddForce(force);

    }

    void Update() {
        if (!hasStarted) {
            hasStarted = true;
        } else {
            if (transform.parent != null) {
                transform.parent = null;
            }
        }


        if (lifeTime <= 0) {
            fade = true;
        } else {
            lifeTime -= Time.deltaTime;
        }

        if (fade) {
            color.a -= fadeRate * Time.deltaTime;
            spriteRenderer.color = color;
            if (spriteRenderer.color.a <= 0) {
                Destroy(gameObject);
            }
        }
    }
}
