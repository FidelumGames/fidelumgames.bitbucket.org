﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Health : MonoBehaviour {

    public int cost = 10;
    public float amount = 0.25f;
    private List<string> descriptions = new List<string>();
    public int descriptionIndex = 0;
    
    private Stats stats;
    private UIManager uiManager;
    private Shop shop;

    void Start() {
        stats = GameObject.FindObjectOfType<Stats>();
        uiManager = GameObject.FindObjectOfType<UIManager>();
        shop = GameObject.FindObjectOfType<Shop>();
        AddDescriptions();
    }

    private void AddDescriptions() {
        descriptions.Add("Small medkit.\n\nHeals %" + (int)(amount * 100) + " of total health");
        descriptions.Add("Medium medkit.\n\nHeals %" + (int)(amount * 100) + " of total health");
        descriptions.Add("Large medkit.\n\nHeals %" + (int)(amount * 100) + " of total health");
        
    }

    public void BuyMe() {
        shop.BuyHealth(this);
        ShowMyInfo();
    }

    public void ShowMyInfo() {
        uiManager.ShowItemInfoPanel(cost, descriptions[descriptionIndex]);
    }
}
