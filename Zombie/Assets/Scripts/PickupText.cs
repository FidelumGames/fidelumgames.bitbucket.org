﻿using UnityEngine;
using System.Collections;

public class PickupText : MonoBehaviour {

    public float ascentSpeed;
    public float opacitySpeed;

    public float xRangeMin;
    public float xRangeMax;
    public float yRangeMin;
    public float yRangeMax;

    public float opacityStart = 1.5f;

    public Vector3 posTemp;

    public TextMesh pickupText;
    public MeshRenderer myRenderer;

    public Color color;

    public string myText;

    void Start() {
        posTemp = transform.position;
        posTemp.x += Random.Range(xRangeMin, xRangeMax);
        posTemp.y += Random.Range(yRangeMin, yRangeMax);
        posTemp.z = -25;
        pickupText = GetComponent<TextMesh>();
        pickupText.text = myText;
        pickupText.color = new Color(0, 0, 0, 0);
    }

    public void setProperties(string text, Color color) {
        myText = text;
        this.color = color;
    }

    void Update() {
        posTemp.y += Time.deltaTime * ascentSpeed;
        transform.position = posTemp;
        opacityStart -= Time.deltaTime * opacitySpeed;

        color.a = opacityStart;
        pickupText.color = color;
        if (pickupText.color.a <= 0) {
            Destroy(gameObject);
        }

    }
}
