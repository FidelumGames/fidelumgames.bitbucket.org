﻿using UnityEngine;
using System.Collections;

public class ZombieTitle : MonoBehaviour {

    private GameObject shooterTitle;
	
	void Start () {
        shooterTitle = GameObject.Find("ShooterTitle");
	}

    public void PlayShooterAnimation(){
        shooterTitle.GetComponent<Animator>().enabled = true;
    }
	
}
