﻿using UnityEngine;
using System.Collections;

public class IntroAnimationInterface : MonoBehaviour {

    private ParticleSystem exhaust;
    public AudioClip dyingSound;
    public AudioClip backfireSound;
    public AudioClip accelerateSound;
    public AudioClip idleSound;
    public AudioClip ignitionSound;

    public GameObject dancingbie;

    private AudioManager audioManager;

    private AudioSource foreignASource;

    private bool reduceSound = false;
    private float startVol, minVol, decayRate;

    private GameObject restart;

    void Start() {
        audioManager = GameObject.FindObjectOfType<AudioManager>();
        exhaust = GetComponentInChildren<ParticleSystem>();
        restart = GameObject.Find("Restart");
        restart.SetActive(false);
    }

    void Update() {
        if (reduceSound) {
            ReduceAudioManagerSFXVolume();
        }
    }

    public void SetExhaustParticleStartSpeed(float speed) {
        exhaust.startSpeed = speed;
    }

    public void StartAnimation() {
        GetComponent<Animator>().enabled = true;
        GameObject.Find("StartButton").SetActive(false);
        GameObject.Find("ClearButton").SetActive(false);
        GameObject.Find("PixelTitle").SetActive(false);
        GameObject.Find("ZombieTitle").SetActive(false);
        GameObject.Find("ShooterTitle").SetActive(false);
    }

    public void EndAnimation() {
        GameObject.FindObjectOfType<StartMenu>().ShowGameElements();
        GameObject.Find("Front Wheel").transform.eulerAngles = Vector3.zero;
        GameObject.Find("Rear Wheel").transform.eulerAngles = Vector3.zero;
        GameObject.Find("PlayerOnBike").SetActive(false);

    }

    public void ChangeEngineSoundToDyingSound() {
        audioManager.PlayClip(dyingSound);
    }

    public void PlayBackfireSound() {
        audioManager.PlayClip(backfireSound);
    }

    public void PlayIgnitionSound() {
        audioManager.PlayClip(ignitionSound);
    }

    public void PlayAccelerationSound() {
        audioManager.PlayClip(accelerateSound);
    }

    public void PlayIdleSound() {
        foreach (AudioSource aSource in GetComponents<AudioSource>()) {
            if (aSource.loop) {
                aSource.clip = idleSound;
                aSource.volume = audioManager.SFXVolume;
                aSource.Play();
                ReduceAudioManagerSFXVolume(aSource);
            }
        }
    }

    public void ReduceAudioManagerSFXVolume(AudioSource aSource) {
        reduceSound = true;
        startVol = audioManager.SFXVolume;
        decayRate = 0.25f;
        minVol = startVol == 0 ? 0 : startVol / 4;
        if (startVol > minVol) {
            startVol = Mathf.Clamp(startVol - decayRate * Time.deltaTime, minVol, Mathf.Infinity);
            aSource.volume = audioManager.SFXVolume = startVol;
        }


        foreignASource = aSource;
    }

    public void ReduceAudioManagerSFXVolume() {
        reduceSound = true;
        if (startVol > minVol) {
            startVol = Mathf.Clamp(startVol - decayRate * Time.deltaTime, minVol, Mathf.Infinity);
            foreignASource.volume = audioManager.SFXVolume = startVol;
        }
    }

    public void EndEngineSound(){
        startVol -= decayRate * Time.deltaTime;
        foreignASource.volume = audioManager.SFXVolume = 0;
    }

    public void ShowCredits() {
        GameObject.FindObjectOfType<Credits>().StartCredits();
    }

    public void ShowRestartButton() {
        restart.SetActive(true);
    }


}
