﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float jumpForce = 12.5f;
    public int jumpsLeft = 3;
    private bool canJump = true;
    public bool canBeKnockedBack = true;
    public bool canMove = true;

    public float knockBackTimer;
    public float knockBackTime = 1f;

    public float disableMoveTime = .5f;
    public float disableMoveTimer;

    private float xInput;

    private string[] animations;
    private string direction;

    private Vector2 mousePos;
    private WeaponController[] weapons;
    private Rigidbody2D myRigidbody;
    private Animator animator;
    private Stats stats;

    private Text frameRateText;

    public float playerMaxXPos;
    public float playerMinXPos;

    void Start() {
        myRigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        stats = GetComponent<Stats>();
        animations = new string[] { "Run", "Idle", "RunLeft", "IdleLeft", "RunBack", "RunBackLeft" };
     //   frameRateText = GameObject.Find("FrameRateText").GetComponent<Text>();

    }

    void Update() {

        CorrectPosition();


        if (canMove) {

            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
                Jump();
            }

            Move();
        } else {
            disableMoveTimer -= Time.deltaTime;


            if (disableMoveTimer <= 0) {
                canMove = true;
            }


        }

        if (knockBackTimer >= 0) {
            knockBackTimer -= Time.deltaTime;
        } else {
            canBeKnockedBack = true;
        }



        DetermineFacingDirection();
        SetAnimationState(GetRequiredAnimation());

      //  UpdateFrameRateText();
    }

    private void CorrectPosition() {
        Vector3 newPos;
        newPos = transform.position;
        
        if (transform.position.x <= playerMinXPos) {
            newPos.x = playerMinXPos;
        } else if (transform.position.x >= playerMaxXPos) {
            newPos.x = playerMaxXPos;
        }

        transform.position = newPos;
    }

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "Ground") {
            canJump = true;
            jumpsLeft = stats.GetMaxJumps();
        }
    }

    void UpdateFrameRateText() {
        frameRateText.text = (int)(1 / Time.deltaTime) + " fps"; ;
    }

    private void Move() {
        xInput = Input.GetAxisRaw("Horizontal");
        myRigidbody.velocity = new Vector2(xInput * stats.GetRunSpeed(), myRigidbody.velocity.y);
    }

    private void Jump() {
        if (canJump) {
            myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
            jumpsLeft--;
        }

        if (jumpsLeft <= 0) {
            canJump = false;
        }
    }

    private string GetRequiredAnimation() {
        if (direction == "Right") {
            if (xInput > 0) {
                return "Run";
            } else if (xInput < 0) {
                return "RunBack";
            } else {
                return "Idle";
            }
        } else {
            if (xInput < 0) {
                return "RunLeft";
            } else if (xInput > 0) {
                return "RunBackLeft";
            } else {
                return "IdleLeft";
            }
        }
    }

    private void SetAnimationState(string state) {
        foreach (string animation in animations) {
            if (animation != state) {
                animator.SetBool(animation, false);
            } else {
                animator.SetBool(animation, true);
            }
        }
    }

    private void DetermineFacingDirection() {
        mousePos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        direction = mousePos.x > transform.position.x ? "Right" : "Left";
    }

    public void KnockBack(GameObject enemy) {

        if (transform.position.x >= playerMinXPos && transform.position.x <= playerMaxXPos) {

            if (canBeKnockedBack) {
                canBeKnockedBack = false;
                canMove = false;
                disableMoveTimer = disableMoveTime;
                knockBackTimer = knockBackTime;
                if (enemy.transform.position.x > transform.position.x) {
                    myRigidbody.velocity = new Vector2(-10, 0);
                } else {
                    myRigidbody.velocity = new Vector2(10, 0);
                }
            }
        }
    }




}
