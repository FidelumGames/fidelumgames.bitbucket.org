﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SpawnerManager : MonoBehaviour {

    private Spawner leftSkinbieSpawner, rightSkinbieSpawner, leftSlenderbieSpawner, rightSlenderbieSpawner, 
        leftCrowSpawner, rightCrowSpawner, leftSkinbieRedSpawner, rightSkinbieRedSpawner, leftBloodBirdSpawner,
        rightBloodBirdSpawner;

    public int currentWave = 0;
    private int spawnedEnemyCount;
    private int livingEnemies;
    private int enemiesRemaining;

    public float timeToNextWave = 0;

    private bool waveHasStarted = false;
    public List<Wave> waves;

    private Text nextWaveText;
    public Text nextWaveButtonText;
    private Text enemiesRemainingText;

    public float damageTakenThisWave = 0;

    private AchievementManager achievementManager;

    private Stats stats;

    private SaveSystem saveSystem;

    

    void Start () {
        saveSystem = GameObject.FindObjectOfType<SaveSystem>();
        stats = GameObject.FindObjectOfType<Stats>();
        achievementManager = GameObject.FindObjectOfType<AchievementManager>();

        nextWaveText = GameObject.Find("NextWave").GetComponent<Text>();
        nextWaveButtonText = nextWaveButtonText ? nextWaveButtonText : GameObject.Find("NextWaveButton").GetComponentInChildren<Text>();
        nextWaveButtonText.GetComponentInParent<Button>().interactable = false;
        enemiesRemainingText = GameObject.Find("EnemiesRemaining").GetComponent<Text>();

        leftSkinbieSpawner = GameObject.Find("LeftSkinbieSpawner").GetComponent<Spawner>();
        rightSkinbieSpawner = GameObject.Find("RightSkinbieSpawner").GetComponent<Spawner>();

        leftSkinbieRedSpawner = GameObject.Find("LeftSkinbieRedSpawner").GetComponent<Spawner>();
        rightSkinbieRedSpawner = GameObject.Find("RightSkinbieRedSpawner").GetComponent<Spawner>();

        leftSlenderbieSpawner = GameObject.Find("LeftSlenderbieSpawner").GetComponent<Spawner>();
        rightSlenderbieSpawner = GameObject.Find("RightSlenderbieSpawner").GetComponent<Spawner>();

        leftCrowSpawner = GameObject.Find("LeftCrowSpawner").GetComponent<Spawner>();
        rightCrowSpawner = GameObject.Find("RightCrowSpawner").GetComponent<Spawner>();

        leftBloodBirdSpawner = GameObject.Find("LeftBloodBirdSpawner").GetComponent<Spawner>();
        rightBloodBirdSpawner = GameObject.Find("RightBloodBirdSpawner").GetComponent<Spawner>();
        StartWave();
    }
	
    public float GetRemainingEnemyPercent() {
        return (enemiesRemaining / waves[currentWave].numEnemiesInWave) * 100;
    }

	void Update () {
        UpdateEnemyCounts();
        enemiesRemaining = !waveHasStarted ? livingEnemies : waves[currentWave].numEnemiesInWave - spawnedEnemyCount + livingEnemies;
        enemiesRemainingText.text = "Enemies remaining: " + enemiesRemaining;

        if (spawnedEnemyCount >= waves[currentWave].numEnemiesInWave && (/*livingEnemies <= 0*/ GetRemainingEnemyPercent() <= 20)) {
            EndWave();
            StopSpawning();
        } 

        if(timeToNextWave >= 0) {
            timeToNextWave -= Time.deltaTime;
        } else if(!waveHasStarted){
            StartWave();
        }

        nextWaveText.text = "Seconds until next wave: " + (int)timeToNextWave;
        
	}

    void UpdateEnemyCounts() {
        spawnedEnemyCount = 0;

        foreach (Spawner spawner in FindObjectsOfType<Spawner>()) {
            spawnedEnemyCount += spawner.GetSpawnedEnemyCount(); 
        }

        livingEnemies = FindObjectsOfType<Enemy>().Length +
            FindObjectsOfType<Crow>().Length;
        
    }

    public void IncreaseDamageTakenThisWave(float amount) {
        damageTakenThisWave += amount;
    }

    void EndWave() {
        achievementManager.UnlockWavesCompleted(currentWave + 1);

        if(damageTakenThisWave == 0) {
            achievementManager.UnlockInvulnerable();
        }

        if(stats.GetHealthRemainingAsPercent() < 11) {
            achievementManager.UnlockCuttingItClose();
        }

        damageTakenThisWave = 0;

        currentWave++;

        if(currentWave % 5 == 0) {
            saveSystem.Save();
        }

        GenerateWaves();

        if (!waves[currentWave].isSpecialWave) {
            waves[currentWave].numEnemiesInWave += currentWave;
        }
        nextWaveButtonText.GetComponentInParent<Button>().interactable = true;
        nextWaveButtonText.text = "Start wave " + (currentWave + 1);
        timeToNextWave = 30f; // waves[currentWave - 1].idleTimeAfterWave;
        StopSpawning();
        foreach (Spawner spawner in FindObjectsOfType<Spawner>()) {
            spawner.ResetSpawnedEnemyCount();
        }
        waveHasStarted = false;
    }

    private void GenerateWaves() {
        while (waves.Count <= currentWave) {
            float rangeMultiplier = .8f;
            float enemiesMultiplier = 1.25f;

            

            for (int i = waves.Count - 10; i < currentWave; i++) {
                waves.Add(waves[i]);
                waves[waves.Count - 1].numEnemiesInWave = (int)(enemiesMultiplier * waves[waves.Count - 1].numEnemiesInWave);

                //LeftSkinbie
                waves[waves.Count - 1].leftSkinbieSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].leftSkinbieSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].leftSkinbieSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].leftSkinbieSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //RightSkinbie
                waves[waves.Count - 1].rightSkinbieSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].rightSkinbieSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].rightSkinbieSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].rightSkinbieSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //RightSkinbieRed
                waves[waves.Count - 1].rightSkinbieRedSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].rightSkinbieRedSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].rightSkinbieRedSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].rightSkinbieRedSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //LeftSkinbieRed
                waves[waves.Count - 1].leftSkinbieRedSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].leftSkinbieRedSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].leftSkinbieRedSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].leftSkinbieRedSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //LeftCrow
                waves[waves.Count - 1].leftCrowSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].leftCrowSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].leftCrowSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].leftCrowSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //RightCrow
                waves[waves.Count - 1].rightCrowSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].rightCrowSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].rightCrowSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].rightCrowSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //LeftBloodBird
                waves[waves.Count - 1].leftBloodBirdSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].leftBloodBirdSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].leftBloodBirdSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].leftBloodBirdSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //RightBloodBird
                waves[waves.Count - 1].rightBloodBirdSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].rightBloodBirdSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].rightBloodBirdSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].rightBloodBirdSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //LeftSlenderbie
                waves[waves.Count - 1].leftSlenderbieSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].leftSlenderbieSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].leftSlenderbieSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].leftSlenderbieSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);

                //RightSlenderbie
                waves[waves.Count - 1].rightSlenderbieSpawnRange.x =
                    Mathf.Clamp(waves[waves.Count - 1].rightSlenderbieSpawnRange.x * rangeMultiplier,
                    .15f, Mathf.Infinity);
                waves[waves.Count - 1].rightSlenderbieSpawnRange.y =
                    Mathf.Clamp(waves[waves.Count - 1].rightSlenderbieSpawnRange.y * rangeMultiplier,
                    .15f, Mathf.Infinity);


            }
        }
    }

    public void UpdateWaveButton() {
        nextWaveButtonText.text = "Start wave " + (currentWave + 1);
        GenerateWaves();
    }

    void StartWave() {
        nextWaveButtonText.GetComponentInParent<Button>().interactable = false;

        waveHasStarted = true;
        leftSkinbieSpawner.SetValues(waves[currentWave].leftSkinbieSpawnRange, waves[currentWave].leftSkinbieSpawnerEnabled);
        rightSkinbieSpawner.SetValues(waves[currentWave].rightSkinbieSpawnRange, waves[currentWave].rightSkinbieSpawnerEnabled);

        leftSkinbieRedSpawner.SetValues(waves[currentWave].leftSkinbieRedSpawnRange, waves[currentWave].leftSkinbieRedSpawnerEnabled);
        rightSkinbieRedSpawner.SetValues(waves[currentWave].rightSkinbieRedSpawnRange, waves[currentWave].rightSkinbieRedSpawnerEnabled);

        leftSlenderbieSpawner.SetValues(waves[currentWave].leftSlenderbieSpawnRange, waves[currentWave].leftSlenderbieSpawnerEnabled);
        rightSlenderbieSpawner.SetValues(waves[currentWave].rightSlenderbieSpawnRange, waves[currentWave].rightSlenderbieSpawnerEnabled);

        leftCrowSpawner.SetValues(waves[currentWave].leftCrowSpawnRange, waves[currentWave].leftCrowSpawnerEnabled);
        rightCrowSpawner.SetValues(waves[currentWave].rightCrowSpawnRange, waves[currentWave].rightCrowSpawnerEnabled);

        leftBloodBirdSpawner.SetValues(waves[currentWave].leftBloodBirdSpawnRange, waves[currentWave].leftBloodBirdSpawnerEnabled);
        rightBloodBirdSpawner.SetValues(waves[currentWave].rightBloodBirdSpawnRange, waves[currentWave].rightBloodBirdSpawnerEnabled);
    }

    void StopSpawning() {
        foreach (Spawner spawner in FindObjectsOfType<Spawner>()) {
            spawner.enabled = false;
        }
    }

    public void UserStartNextWave() {
        timeToNextWave = 0;
    }
    
}
