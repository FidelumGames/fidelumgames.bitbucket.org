﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {

    private AudioSource audioSource;
    private AudioSource sfxSource;
    private Slider musicSlider;
    private Slider SFXSlider;

    public Sprite SFXImage, SFXMuted, musicImage, musicMuted;

    public AudioClip uiClick, errorSound, buySound, track1, track2, track3, track4;

    private Image musicButton, SFXButton;

    public float SFXVolume = .75f;
    private float lastSFXVolume = .75f;
    public float musicVolume = .5f;
    private float lastMusicVolume = .5f;

    void Start() {
        musicButton = GameObject.Find("MusicMute").GetComponent<Image>();
        SFXButton = GameObject.Find("SoundMute").GetComponent<Image>();
        lastSFXVolume = SFXVolume;
        lastMusicVolume = musicVolume;
        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
        SFXSlider = GameObject.Find("SFXSlider").GetComponent<Slider>();
        AssignAudioSources();
    }

    private void AssignAudioSources() {
        foreach (AudioSource aSource in GetComponents<AudioSource>()) {
            if (aSource.loop) {
                audioSource = aSource;
            } else {
                sfxSource = aSource;
            }
        }
    }

    public void PlayClip(AudioClip clip) {
        sfxSource.PlayOneShot(clip, SFXVolume);
    }

    public void PlayUIClick() {
        sfxSource.PlayOneShot(uiClick, SFXVolume);
    }

    public void PlayErrorSound() {
        sfxSource.PlayOneShot(errorSound, SFXVolume);
    }

    public void PlayBuySound() {
        sfxSource.PlayOneShot(buySound, SFXVolume);
    }

    public void SetMusicVolume() {
        musicVolume = musicSlider.value;
        audioSource.volume = musicVolume;
        if (musicVolume != 0) {
            musicButton.sprite = musicImage;
        } else {
            musicButton.sprite = musicMuted;
        }
    }

    public void SetSFXVolume() {
        SFXVolume = SFXSlider.value;
        sfxSource.volume = SFXVolume;
        if (SFXVolume != 0) {
            SFXButton.sprite = SFXImage;
        } else {
            SFXButton.sprite = SFXMuted;
        }
    }

    public void MuteSFX() {
        if (SFXVolume != 0) {
            SFXButton.sprite = SFXMuted;
            lastSFXVolume = SFXVolume;
            SFXVolume = 0;
            SFXSlider.value = 0;
        } else {
            SFXButton.sprite = SFXImage;
            SFXVolume = lastSFXVolume;
            SFXSlider.value = SFXVolume;
        }

        sfxSource.volume = SFXVolume;
    }

    public void MuteMusic() {
        if (musicVolume != 0) {
            musicButton.sprite = musicMuted;
            lastMusicVolume = musicVolume;
            musicVolume = 0;
            musicSlider.value = 0;
        } else {
            musicButton.sprite = musicImage;
            musicVolume = lastMusicVolume;
            musicSlider.value = musicVolume;
        }

        audioSource.volume = musicVolume;
    }

    public void SetTrack(int track) {
        switch (track) {
            case 1:
                audioSource.clip = track1;
                break;
            case 2:
                audioSource.clip = track2;
                break;
            case 3:
                audioSource.clip = track3;
                break;
            case 4:
                audioSource.clip = track4;
                break;
        }
        audioSource.Play();
    }


}
