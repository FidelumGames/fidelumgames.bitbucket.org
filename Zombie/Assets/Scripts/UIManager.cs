﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class UIManager : MonoBehaviour {

    GameObject[] UIs;
    Stats stats;


    //Shop Info Panel
    public GameObject infoPanel;
    public Text infoPanelText;
    public Text infoPanelCashText;

    //Weapon Shop Info Panel
    public GameObject weaponInfoPanel;
    public GameObject weaponBuyButton;
    private Text weaponInfoPanelText;
    private Text weaponInfoPanelCashText;
    private Text weaponInfoPanelNameText;
    private WeaponButton activeWeaponButton;

    //Weapon Upgrade Panel
    public GameObject upgradeInfoPanel;
    public GameObject upgradeBuyButton;
    private Text upgradeInfoPanelText;
    private Text upgradeInfoPanelCashText;
    private Text upgradeInfoPanelNameText;
    private UpgradeButton activeUpgradeButton;
    private List<GameObject> upgradeButtons;
    private List<GameObject> upgradeGroups;
    private GameObject upgradeSubPanel;

    //Ability Upgrade Panel
    public GameObject abilityInfoPanel;
    public Text abilityInfoPanelText;

    public Text abilityPointsText;

    public GameObject UITextPrefab;
    private PickupTextUI UIText;
    private HealthSlider healthSlider;
    private XPSlider xpSlider;
    private CashText cashText;

    public enum UIColor {
        Green, Red, Yellow, Blue
    }

    private AudioManager audioManager;

    public Vector2 infoPanelOffset = new Vector2(10, 10);

    public GameObject achievementInfoPanel;
    public Text infoPanelTitle;
    public Text infoPanelDescription;
    public Text infoPanelProgress;
    public Text infoPanelReward;

    public void GetAchievementPanelComponents() {
        achievementInfoPanel = GameObject.Find("AchievementInfo");
        infoPanelTitle = GameObject.Find("AchievementInfo/TitleText").GetComponent<Text>();
        infoPanelDescription = GameObject.Find("AchievementInfo/DescriptionText").GetComponent<Text>();
        infoPanelProgress = GameObject.Find("AchievementInfo/ProgressText").GetComponent<Text>();
        infoPanelReward = GameObject.Find("AchievementInfo/RewardText").GetComponent<Text>();
    }

    void Start() {

        GetAchievementPanelComponents();        

        audioManager = GameObject.FindObjectOfType<AudioManager>();
        UIs = GameObject.FindGameObjectsWithTag("UIMenu");
        stats = GameObject.FindObjectOfType<Stats>();

        infoPanel = GameObject.Find("InfoPanel");
        infoPanelText = GameObject.Find("InfoPanelText").GetComponent<Text>();
        infoPanelCashText = GameObject.Find("InfoPanelCostText").GetComponent<Text>();

        weaponInfoPanel = GameObject.Find("WeaponInfoPanel");
        weaponInfoPanelText = GameObject.Find("WeaponInfoPanelText").GetComponent<Text>();
        weaponInfoPanelCashText = GameObject.Find("WeaponInfoPanelCostText").GetComponent<Text>();
        weaponInfoPanelNameText = GameObject.Find("WeaponInfoPanelNameText").GetComponent<Text>();
        weaponBuyButton = GameObject.Find("WeaponBuyButton");

        upgradeInfoPanel = GameObject.Find("WeaponUpgradePanel");
        upgradeBuyButton = GameObject.Find("UpgradeBuyButton");
        upgradeInfoPanelText = GameObject.Find("UpgradeInfoPanelText").GetComponent<Text>();
        upgradeInfoPanelCashText = GameObject.Find("UpgradeInfoPanelCostText").GetComponent<Text>();
        upgradeInfoPanelNameText = GameObject.Find("UpgradeInfoPanelNameText").GetComponent<Text>();
        upgradeButtons = new List<GameObject>();
        upgradeGroups = GameObject.FindGameObjectsWithTag("UpgradeGroup").ToList<GameObject>();
        upgradeSubPanel = GameObject.Find("UpgradeSubPanel");

        abilityInfoPanel = GameObject.Find("AbilityInfoPanel");
        abilityInfoPanelText = GameObject.Find("AbilityInfoPanelText").GetComponent<Text>();
        abilityInfoPanel.SetActive(false);

        abilityPointsText = GameObject.Find("AbilityPointsText").GetComponent<Text>();

        healthSlider = GameObject.FindObjectOfType<HealthSlider>();
        xpSlider = GameObject.FindObjectOfType<XPSlider>();
        cashText = GameObject.FindObjectOfType<CashText>();

        foreach (UpgradeButton obj in GameObject.FindObjectsOfType<UpgradeButton>()) {
            upgradeButtons.Add(obj.gameObject);
        }

        SetActiveUI("None");
        HideInfoPanel();
        UpdateUI();
    }

    void Update() {

        abilityPointsText.text = stats.skillPoints + "";

        if (GameObject.FindObjectOfType<Stats>() && ( Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.I))) {
            activeWeaponButton = null;
            HideAbilityInfoPanel();
            ToggleUI();
            if (!achievementInfoPanel) {
                GetAchievementPanelComponents();
            }
            achievementInfoPanel.SetActive(false);
        }

        if (!activeWeaponButton) {
            weaponInfoPanel.SetActive(false);
        }

        UpdateUI();


    }

    public void ShowItemInfoPanel(int cost, string description) {
        infoPanelCashText.text = "Cost:  " + cost.ToString();
        if (cost > stats.cash) {
            infoPanelCashText.color = Color.red;
        } else {
            infoPanelCashText.color = Color.white;
        }

        infoPanelText.text = description;
        if (!infoPanel.activeSelf) {
            audioManager.PlayUIClick();
        }
        infoPanel.SetActive(true);
        weaponInfoPanel.SetActive(false);
        activeWeaponButton = null;
    }

    public void BuyActiveWeaponButton() {
        activeWeaponButton.BuyMe();
        weaponBuyButton.SetActive(!activeWeaponButton.myWeapon.isUnlocked);
        SetActiveWeaponButton(activeWeaponButton);
    }

    public void ShowWeaponInfoPanel(string name, int cost, string description) {
        if (!activeWeaponButton) {
            if (cost == 0) {
                weaponInfoPanelCashText.text = "Owned";
            } else {
                weaponInfoPanelCashText.text = "Cost: " + cost.ToString();
                if (cost > stats.cash) {
                    weaponInfoPanelCashText.color = Color.red;
                } else {
                    weaponInfoPanelCashText.color = Color.white;
                }
            }
            weaponInfoPanelNameText.text = name;

            weaponInfoPanelText.text = description;
            if (!weaponInfoPanel.activeSelf) {
                audioManager.PlayUIClick();
            }
            weaponInfoPanel.SetActive(true);
        }
    }

    public void ShowAbilityInfoPanel(string info) {
        abilityInfoPanel.SetActive(true);
        abilityInfoPanelText.text = info;
    }

    public void HideAbilityInfoPanel() {
        abilityInfoPanel.SetActive(false);
    }

    public void SetActiveWeaponButton(WeaponButton button) {
        activeWeaponButton = null;
        ShowWeaponInfoPanel(button.transform.name, button.myWeapon.isUnlocked ? 0 : button.cost, button.myDescription);
        activeWeaponButton = button;
        if (activeWeaponButton.myWeapon.isUnlocked) {
            weaponBuyButton.SetActive(false);
            upgradeInfoPanel.SetActive(true);

            upgradeBuyButton.SetActive(activeUpgradeButton != null);

            foreach (GameObject obj in upgradeGroups) {
                if (obj.name != activeWeaponButton.myWeapon.name + "UpgradeGroup") {
                    obj.SetActive(false);
                } else {
                    obj.SetActive(true);
                }
            }
        } else {
            weaponBuyButton.SetActive(true);
            upgradeInfoPanel.SetActive(false);
        }
    }

    public void SetActiveUpgradeButton(UpgradeButton upgradeButton) {
        activeUpgradeButton = upgradeButton;
        ShowUpgradeInfoPanel();
        upgradeBuyButton.SetActive(activeUpgradeButton != null);
        ShowUpgradeInfoPanel();
    }

    public void ShowUpgradeInfoPanel() {

        string upgrade = activeUpgradeButton.upgrade;
        int currLevel = activeUpgradeButton.currLevel;
        int maxLevel = activeUpgradeButton.maxLevel;
        int[] costs = activeUpgradeButton.costs;
        float[] levelValues = activeUpgradeButton.levelValues;

        upgradeSubPanel.SetActive(true);
        if (currLevel >= maxLevel) {
            upgradeBuyButton.SetActive(false);

        }
        upgradeInfoPanelNameText.text = upgrade + " upgrade";

        if (currLevel < maxLevel) {
            upgradeInfoPanelCashText.text = "Cost: " + costs[currLevel];
            upgradeInfoPanelText.text = "Current " + upgrade + ": " +
            activeUpgradeButton.GetCurrentStatValueAsString() +
            "\nNext Level: " + activeUpgradeButton.GetNextStatValueAsString();
            if (costs[currLevel] > stats.cash) {
                upgradeInfoPanelCashText.color = Color.red;
            } else {
                upgradeInfoPanelCashText.color = Color.white;
            }
        } else {
            upgradeInfoPanelCashText.text = "Cost: Fully Upgraded";
            upgradeInfoPanelText.text = "Current " + upgrade + ": " + activeUpgradeButton.GetCurrentStatValueAsString();
        }
    }

    public void BuyActiveUpgradeButton() {
        activeUpgradeButton.BuyMe();
        GameObject.FindObjectOfType<WeaponChanger>().UpdateStats();
    }

    public void HideInfoPanel() {
        if (!achievementInfoPanel) {
            GetAchievementPanelComponents();
        }
        achievementInfoPanel.SetActive(false);
        infoPanel.SetActive(false);
        upgradeSubPanel.SetActive(false);
        if (!activeWeaponButton) {
            weaponInfoPanel.SetActive(false);
            upgradeInfoPanel.SetActive(false);
            activeWeaponButton = null;
        }
    }

    public void SetActiveUI(string myUI) {
        foreach (GameObject ui in UIs) {
            if (ui.name == myUI) {
                ui.SetActive(true);
            } else {
                ui.SetActive(false);
            }
        }

        activeWeaponButton = null;
    }

    public void ToggleUI() {
        Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        activeWeaponButton = null;
        foreach (GameObject ui in UIs) {
            if (ui.activeSelf) {

                ui.SetActive(false);
                infoPanel.SetActive(false);
                return;
            }
        }

        SetActiveUI("ShopUI");

        weaponInfoPanel.SetActive(false);
        HideInfoPanel();
        HideAbilityInfoPanel();
    }

   

    public void ShowUIText(string message, UIColor color, string textPosObjName) {

        Color textColor = Color.white;
        Vector2 textPos = Vector2.zero;
        float offsetMultiplier = 1.0f;

        switch (color) {
            case UIColor.Green:
                textColor = Color.green;
                break;
            case UIColor.Red:
                textColor = Color.red;
                break;
            case UIColor.Yellow:
                textColor = new Color(141, 96, 0);
                break;
            case UIColor.Blue:
                textColor = Color.blue;
                break;
        }

        if (textPosObjName == "mouse") {
            textPos = Input.mousePosition;
        } else {
            textPos = GameObject.Find(textPosObjName).transform.position;
            offsetMultiplier = 0.5f;
        }

        UIText = ((GameObject)Instantiate(UITextPrefab, textPos,
            Quaternion.identity)).GetComponent<PickupTextUI>();
        UIText.transform.SetParent(GameObject.Find("Canvas").transform);

        textPos.x += UIText.GetComponent<RectTransform>().sizeDelta.x * offsetMultiplier;

        UIText.transform.position = textPos;
        UIText.setProperties(message, textColor);
    }

    public void UpdateUI() {
        healthSlider.UpdateUI();
        xpSlider.UpdateUI();
        cashText.UpdateUI();
    }
}
