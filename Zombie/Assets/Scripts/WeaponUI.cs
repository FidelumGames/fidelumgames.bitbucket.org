﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponUI : MonoBehaviour {

    public Text text;
    public Slider slider;
    public Image weaponThumbnail;
    public Stats stats;
    private WeaponChanger weaponChanger;
    void Start() {
       // text = GetComponentInChildren<Text>();
        slider = GetComponent<Slider>();
       // weaponThumbnail = GameObject.Find("WeaponButton").GetComponent<Image>();
        weaponChanger = GameObject.FindObjectOfType<WeaponChanger>();
       // stats = GameObject.FindObjectOfType<Stats>();
        
    }

    void Update() {
        if (weaponChanger) {
            UpdateUI(weaponChanger.GetCurrentWeapon());
        }
    }

    public void UpdateUI(WeaponController weapon) {
        weaponThumbnail.sprite = weapon.icon;
        slider = slider ? slider : GetComponent<Slider>();
        slider.value = (float)weapon.roundsLoaded / (float)weapon.capacity;
        UpdateText(stats.GetRoundsOwned(weapon.caliber), weapon.capacity, weapon.roundsLoaded);
    }

    void UpdateText(int ammoOwned, int weaponCapacity, int ammoLoaded) {
        text.text = ammoLoaded + "/" + weaponCapacity + " (" + ammoOwned + ")";
    }
}
