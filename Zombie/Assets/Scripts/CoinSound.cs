﻿using UnityEngine;
using System.Collections;

public class CoinSound : MonoBehaviour {

    private AudioSource aSource;

    public float minPitch;
    public float maxPitch;
    public bool autoDestroy = true;
    private float actualPitch;

    
    void Start () {
        actualPitch = Random.Range(minPitch, maxPitch);
        aSource = GetComponent<AudioSource>();
        aSource.pitch = actualPitch;
        aSource.volume = GameObject.FindObjectOfType<AudioManager>().SFXVolume;
	}

    void Update() {
        if (!aSource.isPlaying && autoDestroy) {
            Destroy(gameObject);
        }
    }
	
	
	
}
