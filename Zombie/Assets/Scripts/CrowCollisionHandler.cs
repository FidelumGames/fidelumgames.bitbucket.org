﻿using UnityEngine;
using System.Collections;

public class CrowCollisionHandler : MonoBehaviour {

    GameObject crowParent;
    void Start() {
        crowParent = transform.parent.gameObject;
    }

    void OnTriggerEnter2D(Collider2D col) {
        
        if (col.GetComponent<Stats>()) {
           crowParent.GetComponent<Animator>().SetTrigger("attack");
            crowParent.GetComponent<Crow>().isAttacking = true;
            GetComponent<Animator>().SetBool("dive", false);
        }
    }
}
