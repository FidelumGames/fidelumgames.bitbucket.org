﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class XPSlider : MonoBehaviour {

    private Text text;
    private Slider slider;
    private Stats stats;

	// Use this for initialization
	void Start () {
        text = GetComponentInChildren<Text>();
        slider = GetComponentInChildren<Slider>();
        stats = GameObject.FindObjectOfType<Stats>();
    }	

    public void UpdateUI() {
        text = GetComponentInChildren<Text>();
        slider = GetComponentInChildren<Slider>();
        stats = GameObject.FindObjectOfType<Stats>();
        if (stats) {
            text.text = stats.IsMaxLevel() ? "MAX" : (int)stats.totalXP + "/" + (int)stats.toNextLevel + " (" +
                (int)(stats.totalXP / stats.toNextLevel * 100f) + "%)";
            slider.value = stats.IsMaxLevel() ? 1 : stats.totalXP / stats.toNextLevel;
        }
    }    
}
