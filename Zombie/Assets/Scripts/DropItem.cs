﻿using UnityEngine;

[System.Serializable]
public class DropItem {
    public GameObject dropItem;
    public float chance;	
}
