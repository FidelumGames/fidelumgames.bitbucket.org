﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour {
    private AchievementManager achievementManager;

    public enum EnemyType {
        Skinbie, Slenderbie
    };

    public SeveredLimb leftArmPrefab, rightArmPrefab, headPrefab,
        leftLegPrefab, rightLegPrefab, chestPrefab, hipsPrefab;
    public GameObject leftArmPos, rightArmPos, headPos, leftLegPos, rightLegPos, chestPos, hipsPos;

    public EnemyType enemyType;

    private enum Limb {
        Right = 1, Head = 2, Left = 4
    };

    private List<Limb> limbs = new List<Limb>();
    private int limbSum = 7;

    public float health;
    public int XP = 10;

    public bool knocksBack = false;

    public float damage = 1f;

    public float limbLossChance = 25;

    public float moveThreshold = 1f;

    public float minMoveSpeed, maxMoveSpeed;

    private float moveSpeed = 5f;

    private float attackDelay = .25f;
    public float delayBase = .25f;

    public GameObject pickupText;
    private PickupText newPickupText;

    private Vector3 startScale;
    private Vector3 scaleTemp;

    private Stats target;
    private PlayerController playerTarget;
    private GameObject player;

    public DropItem[] dropItems;

    Animator animator;

    public AudioClip[] enemySounds;
    private AudioSource aSource;

    public float soundDelayMin;
    public float soundDelayMax;
    private float timeToNextSound;

    private AudioManager audioManager;
    private SpawnerManager spawnerManager;

    public float healthFactor;

    void Start() {
        spawnerManager = GameObject.FindObjectOfType<SpawnerManager>();
        audioManager = GameObject.FindObjectOfType<AudioManager>();
        achievementManager = GameObject.FindObjectOfType<AchievementManager>();

        timeToNextSound = Random.Range(soundDelayMin, soundDelayMax);

        aSource = GetComponent<AudioSource>();

        name = name + GetInstanceID();

        leftArmPos = GameObject.Find(name + "/LeftArmPos");
        rightArmPos = GameObject.Find(name + "/RightArmPos");
        headPos = GameObject.Find(name + "/HeadPos");
        leftLegPos = GameObject.Find(name + "/LeftLegPos");
        rightLegPos = GameObject.Find(name + "/RightLegPos");
        chestPos = GameObject.Find(name + "/ChestPos");
        hipsPos = GameObject.Find(name + "/HipsPos");


        moveSpeed = Random.Range(minMoveSpeed, maxMoveSpeed);

        animator = GetComponent<Animator>();

        if (enemyType == EnemyType.Skinbie) {

            limbs.Add(Limb.Head);
            limbs.Add(Limb.Left);
            limbs.Add(Limb.Right);

            int numLimbsToRemove = Random.Range(0, 4);

            for (int i = 0; i < numLimbsToRemove; i++) {
                int limbToRemove = Random.Range(0, limbs.Count);
                RemoveSkinbieLimb(limbs[limbToRemove], false);
            }
        }


        attackDelay = 0;
        player = GameObject.Find("Player");
        startScale = transform.localScale;
        scaleTemp = startScale;
        scaleTemp.x = -startScale.x;

        SetItemChances();
        SetNextEnemySound();
        IncreaseHealth();
    }

    private void IncreaseHealth() {
        health += spawnerManager.currentWave * healthFactor;
    }

    private void SetNextEnemySound() {
        int index = Random.Range(0, enemySounds.Length);
        audioManager.PlayClip(enemySounds[index]);
    }

    void Update() {
        if (player) {
            Attack();
            Move();
        }

        CountDownToEnemySound();
    }

    private void CountDownToEnemySound() {
        if (timeToNextSound <= 0) {
            timeToNextSound = Random.Range(soundDelayMin, soundDelayMax);
            SetNextEnemySound();

        } else {
            timeToNextSound -= Time.deltaTime;
        }
    }


    private void RemoveSkinbieLimb(Limb limb, bool spawnLimb) {
        limbSum -= (int)limb;
        animator.SetInteger("LimbNumber", limbSum);

        if (limb == Limb.Head) {
            this.transform.GetChild(0).gameObject.SetActive(false);
        }

        if (spawnLimb && limbs.Count > 0) {
            achievementManager.RemoveLimb();
            switch (limb) {
                case Limb.Head:
                    Instantiate(headPrefab,
                        headPos.transform.position,
                        Quaternion.identity);
                    break;
                case Limb.Left:
                    Instantiate(leftArmPrefab,
                        leftArmPos.transform.position,
                        Quaternion.identity);
                    break;
                case Limb.Right:
                    Instantiate(rightArmPrefab,
                        rightArmPos.transform.position,
                        Quaternion.identity);
                    break;
            }
        }
        limbs.Remove(limb);
    }

    void Move() {
        if (Mathf.Abs(player.transform.position.x - transform.position.x) > moveThreshold) {
            if (player.transform.position.x > transform.position.x) {
                transform.localScale = scaleTemp;
            } else {
                transform.localScale = startScale;
            }
            transform.Translate(Vector3.left * Time.deltaTime * moveSpeed);
        }
    }

    void Attack() {
        if (target && attackDelay <= 0) {

            target.TakeDamage(damage);
            attackDelay = delayBase;
        }

        attackDelay -= Time.deltaTime;
    }

    public void TakeDamage(float damage) {
        health -= damage;
        CheckForLimbLoss();
        newPickupText = ((GameObject)Instantiate(pickupText, (Vector2)transform.position,
            Quaternion.identity)).GetComponent<PickupText>();
        newPickupText.setProperties("-" + (int)damage, Color.white);
        if (health <= 0) {
            Die();
        }
    }

    private void CheckForLimbLoss() {
        if (enemyType == EnemyType.Skinbie) {

            float roll = Random.Range(0, 100);

            if (roll <= limbLossChance && limbs.Count > 0) {
                int limbToRemove = Random.Range(0, limbs.Count);
                RemoveSkinbieLimb(limbs[limbToRemove], true);
            }
        }
    }

    public void EndDie() {
        Explode();
        Destroy(gameObject);
    }

    void Die() {
        achievementManager.IncreaseKills();
        target = player.GetComponent<Stats>();

        target.GainXP(XP);
        newPickupText = ((GameObject)Instantiate(pickupText, (Vector2)transform.position,
            Quaternion.identity)).GetComponent<PickupText>();
        newPickupText.setProperties("+" + (int)target.GetTotalXPGainedInt(XP) + " XP", Color.blue);
        DropItem();
        if (GameObject.FindObjectOfType<WeaponChanger>().GetCurrentWeapon().numProjectiles > 1 ||
            GameObject.FindObjectOfType<WeaponChanger>().GetCurrentWeapon().gameObject.name == "Minigun") {
            Explode();
        }
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D col) {
        target = col.GetComponent<Stats>();
        playerTarget = col.GetComponent<PlayerController>();
        if (!playerTarget) {
            playerTarget = col.GetComponentInParent<PlayerController>();
        }
        if (knocksBack && playerTarget) {
            playerTarget.KnockBack(gameObject);
        }
    }

    void OnTriggerStay2D(Collider2D col) {
        target = col.GetComponent<Stats>();
        playerTarget = col.GetComponent<PlayerController>();
        if (!playerTarget) {
            playerTarget = col.GetComponentInParent<PlayerController>();
        }
        if (knocksBack && playerTarget) {

            playerTarget.KnockBack(gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.GetComponent<PlayerController>()) {
            target = null;
            playerTarget = null;
        }
    }

    private void DropItem() {
        float roll = Random.Range(0, 100);

        for (int i = 0; i < dropItems.Length; i++) {
            if (roll <= dropItems[i].chance) {
                Instantiate((GameObject)dropItems[i].dropItem, transform.position, Quaternion.identity);
                return;
            }
        }
    }

    private void SetItemChances() {

        float sum = 0;
        for (int i = 0; i < dropItems.Length; i++) {
            sum += dropItems[i].chance;
            if (i != 0) {
                dropItems[i].chance = dropItems[i - 1].chance + dropItems[i].chance;
            }
        }

        if (sum > 100) {
       //     Debug.LogWarning(gameObject.name + ": The sum of item drop chances must be <= 100. It is currently " + sum);
        }
    }

    private void Explode() {
        if (enemyType != EnemyType.Slenderbie) {
            achievementManager.IncreaseExplodes();
            if (limbs.Contains(Limb.Head)) {
                Instantiate(headPrefab,
                           headPos.transform.position,
                           Quaternion.identity);
            }

            if (limbs.Contains(Limb.Left)) {
                Instantiate(leftArmPrefab,
                           leftArmPos.transform.position,
                           Quaternion.identity);
            }

            if (limbs.Contains(Limb.Right)) {
                Instantiate(rightArmPrefab,
                           rightArmPos.transform.position,
                           Quaternion.identity);
            }

            Instantiate(leftLegPrefab, leftLegPos.transform.position, Quaternion.identity);
            Instantiate(rightLegPrefab, rightLegPos.transform.position, Quaternion.identity);
            Instantiate(chestPrefab, chestPos.transform.position, Quaternion.identity);

        } else {
            Instantiate(leftLegPrefab, leftLegPos.transform.position, Quaternion.identity);
            Instantiate(rightLegPrefab, rightLegPos.transform.position, Quaternion.identity);
            Instantiate(chestPrefab, chestPos.transform.position, Quaternion.identity);
            Instantiate(leftArmPrefab, leftArmPos.transform.position, Quaternion.identity);
            Instantiate(rightArmPrefab, rightArmPos.transform.position, Quaternion.identity);
            Instantiate(hipsPrefab, hipsPos.transform.position, Quaternion.identity);
            Instantiate(headPrefab, headPos.transform.position, Quaternion.identity);
            
        }
    }
}
    
