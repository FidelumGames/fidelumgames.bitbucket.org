﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Wave {

    public string description;
    public float idleTimeAfterWave = 30f;
    public int numEnemiesInWave = 5;

    public bool leftSkinbieSpawnerEnabled = false;
    public Vector2 leftSkinbieSpawnRange = new Vector2(.5f, 3);

    public bool rightSkinbieSpawnerEnabled = false;
    public Vector2 rightSkinbieSpawnRange = new Vector2(.5f, 3);

    public bool leftSkinbieRedSpawnerEnabled = false;
    public Vector2 leftSkinbieRedSpawnRange = new Vector2(.5f, 3);

    public bool rightSkinbieRedSpawnerEnabled = false;
    public Vector2 rightSkinbieRedSpawnRange = new Vector2(.5f, 3);

    public bool leftSlenderbieSpawnerEnabled = false;
    public Vector2 leftSlenderbieSpawnRange = new Vector2(.5f, 3);

    public bool rightSlenderbieSpawnerEnabled = false;
    public Vector2 rightSlenderbieSpawnRange = new Vector2(.5f, 3);

    public bool leftCrowSpawnerEnabled = false;
    public Vector2 leftCrowSpawnRange = new Vector2(.5f, 3);

    public bool rightCrowSpawnerEnabled = false;
    public Vector2 rightCrowSpawnRange = new Vector2(.5f, 3);

    public bool leftBloodBirdSpawnerEnabled = false;
    public Vector2 leftBloodBirdSpawnRange = new Vector2(.5f, 3);

    public bool rightBloodBirdSpawnerEnabled = false;
    public Vector2 rightBloodBirdSpawnRange = new Vector2(.5f, 3);

    public bool isSpecialWave = false;

}
   

