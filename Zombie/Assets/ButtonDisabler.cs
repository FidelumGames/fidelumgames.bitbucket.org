﻿using UnityEngine;
using System.Collections;

public class ButtonDisabler : MonoBehaviour {


    public string[] nameChecks;

    // Use this for initialization
    void Start() {
        gameObject.SetActive(!CheckIfUnlocked());
    }

    private bool CheckIfUnlocked() {
        bool allUnlocked = true;

        foreach (string key in nameChecks) {
            if (PlayerPrefs.GetInt(key) != 1) {
                allUnlocked = false;
            }
        }

        return allUnlocked;
    }
}
