﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class SaveSystem : MonoBehaviour {

    private Stats stats;
    private AchievementButton[] achievementButtons;
    private AchievementManager achievementManager;
    private Motorbike motorbike;
    private AbilityButton[] abilityButtons;
    private WeaponChanger weaponChanger;
    private WeaponController[] weapons;
    private UpgradeButton[] weaponUpgrades;
    private SpawnerManager spawnerManager;

    public Sprite purchasedSprite;

    void Start() {
        stats = GameObject.FindObjectOfType<Stats>();
        achievementButtons = GameObject.FindObjectsOfType<AchievementButton>();
        achievementManager = GameObject.FindObjectOfType<AchievementManager>();
        motorbike = GameObject.FindObjectOfType<Motorbike>();
        abilityButtons = GameObject.FindObjectsOfType<AbilityButton>();
        weaponChanger = GameObject.FindObjectOfType<WeaponChanger>();
        weapons = GameObject.FindObjectsOfType<WeaponController>();
        weaponUpgrades = GameObject.FindObjectsOfType<UpgradeButton>();
        spawnerManager = GameObject.FindObjectOfType<SpawnerManager>();
        if (PlayerPrefs.HasKey("currentWave")) {
            Load();
        }
    }

    public void Save() {
        SavePlayerStats();
        SaveAchievements();
        SaveMotorbikeStats();
        SaveAchievementManagerStats();
        SaveAbilityButtonStats();
        SaveWeaponChangerStats();
        SaveWeaponStats();
        SaveWeaponUpgrades();
        SaveSpawnStats();
        PlayerPrefs.Save();
    }

    public void Load() {
        LoadPlayerStats();
        LoadAchievements();
        LoadMotorbikeStats();
        LoadAchievementManagerStats();
        LoadAbilityButtonStats();
        LoadWeaponChangerStats();
        LoadWeaponStats();
        LoadWeaponUpgrades();
        LoadSpawnStats();
    }

    private void SavePlayerStats() {
        PlayerPrefs.SetInt("maxJumps", stats.maxJumps);
        PlayerPrefs.SetInt("cash", stats.cash);
        PlayerPrefs.SetFloat("moneyMultiplier", stats.moneyMultiplier);
        PlayerPrefs.SetFloat("damageResistance", stats.damageResistance);
        PlayerPrefs.SetFloat("regenAmount", stats.regenAmount);
        PlayerPrefs.SetFloat("reloadModifier", stats.reloadModifier);
        PlayerPrefs.SetFloat("accuracyModifier", stats.accuracyModifier);
        PlayerPrefs.SetFloat("damageModifier", stats.damageModifier);
        PlayerPrefs.SetInt("skillPoints", stats.skillPoints);
        PlayerPrefs.SetFloat("healthGainMultiplier", stats.healthGainMultiplier);
        PlayerPrefs.SetFloat("ammoMultiplier", stats.ammoMultiplier);
        PlayerPrefs.SetFloat("magnetStrength", stats.magnetStrength);
        PlayerPrefs.SetFloat("runSpeed", stats.runSpeed);
        PlayerPrefs.SetFloat("repair", stats.repair);
        PlayerPrefs.SetFloat("repairModifier", stats.repairModifier);
        PlayerPrefs.SetFloat("health", stats.health);
        PlayerPrefs.SetFloat("healthMax", stats.healthMax);
        PlayerPrefs.SetFloat("totalXP", stats.totalXP);
        PlayerPrefs.SetFloat("XPMultiplier", stats.XPMultiplier);
        PlayerPrefs.SetInt("playerAttackDamage", stats.playerAttackDamage);
        PlayerPrefs.SetInt("toNextLevel", stats.toNextLevel);
        PlayerPrefs.SetInt("currLevel", stats.currLevel);
        PlayerPrefs.SetInt("twentyTwoRounds", stats.twentyTwoRounds);
        PlayerPrefs.SetInt("nineMMRounds", stats.nineMMRounds);
        PlayerPrefs.SetInt("twelveGaRounds", stats.twelveGaRounds);
        PlayerPrefs.SetInt("fiftyCalRounds", stats.fiftyCalRounds);
        PlayerPrefs.SetInt("sevenSixTwoRounds", stats.sevenSixTwoRounds);
        PlayerPrefs.SetInt("fiveFiveSixRounds", stats.fiveFiveSixRounds);
        PlayerPrefs.SetInt("fiveMMRounds", stats.fiveMMRounds);
        PlayerPrefs.SetInt("tenMMRounds", stats.tenMMRounds);

    }

    private void LoadPlayerStats() {
        stats.maxJumps = PlayerPrefs.GetInt("maxJumps");
        stats.cash = PlayerPrefs.GetInt("cash");
        stats.moneyMultiplier = PlayerPrefs.GetFloat("moneyMultiplier");
        stats.damageResistance = PlayerPrefs.GetFloat("damageResistance");
        stats.regenAmount = PlayerPrefs.GetFloat("regenAmount");
        stats.reloadModifier = PlayerPrefs.GetFloat("reloadModifier");
        stats.accuracyModifier = PlayerPrefs.GetFloat("accuracyModifier");
        stats.damageModifier = PlayerPrefs.GetFloat("damageModifier");
        stats.skillPoints = PlayerPrefs.GetInt("skillPoints");
        stats.healthGainMultiplier = PlayerPrefs.GetFloat("healthGainMultiplier");
        stats.ammoMultiplier = PlayerPrefs.GetFloat("ammoMultiplier");
        stats.magnetStrength = PlayerPrefs.GetFloat("magnetStrength");
        stats.runSpeed = PlayerPrefs.GetFloat("runSpeed");
        stats.repair = PlayerPrefs.GetFloat("repair");
        stats.repairModifier = PlayerPrefs.GetFloat("repairModifier");
        stats.health = PlayerPrefs.GetFloat("health");
        stats.healthMax = PlayerPrefs.GetFloat("healthMax");
        stats.totalXP = PlayerPrefs.GetFloat("totalXP");
        stats.XPMultiplier = PlayerPrefs.GetFloat("XPMultiplier");
        stats.playerAttackDamage = PlayerPrefs.GetInt("playerAttackDamage");
        stats.toNextLevel = PlayerPrefs.GetInt("toNextLevel");
        stats.currLevel = PlayerPrefs.GetInt("currLevel");
        stats.twentyTwoRounds = PlayerPrefs.GetInt("twentyTwoRounds");
        stats.nineMMRounds = PlayerPrefs.GetInt("nineMMRounds");
        stats.twelveGaRounds = PlayerPrefs.GetInt("twelveGaRounds");
        stats.fiftyCalRounds = PlayerPrefs.GetInt("fiftyCalRounds");
        stats.sevenSixTwoRounds = PlayerPrefs.GetInt("sevenSixTwoRounds");
        stats.fiveFiveSixRounds = PlayerPrefs.GetInt("fiveFiveSixRounds");
        stats.fiveMMRounds = PlayerPrefs.GetInt("fiveMMRounds");
        stats.tenMMRounds = PlayerPrefs.GetInt("tenMMRounds");
    }

    private void SaveAchievements() {
        foreach (AchievementButton achievement in achievementButtons) {
            PlayerPrefs.SetInt(achievement.name, achievement.IsUnlocked);
        }
    }

    private void LoadAchievements() {
        foreach (AchievementButton achievement in achievementButtons) {
            achievement.IsUnlocked = PlayerPrefs.GetInt(achievement.name);
        }
    }

    private void SaveAchievementManagerStats() {
        PlayerPrefs.SetInt("unlockedWeapons", achievementManager.unlockedWeapons);
        PlayerPrefs.SetInt("upgradedAbilities", achievementManager.upgradedAbilities);
        PlayerPrefs.SetInt("numKills", achievementManager.numKills);
        PlayerPrefs.SetInt("killsUnlocked", achievementManager.killsUnlocked);
        PlayerPrefs.SetInt("limbsUnlocked", achievementManager.limbsUnlocked);
        PlayerPrefs.SetInt("damageTakenUnlocked", achievementManager.damageTakenUnlocked);
        PlayerPrefs.SetInt("repairUnlocked", achievementManager.repairUnlocked);
        PlayerPrefs.SetInt("earnedCash", achievementManager.earnedCash);
        PlayerPrefs.SetInt("wavesUnlocked", achievementManager.wavesUnlocked);
        PlayerPrefs.SetInt("loadedUnlocked", achievementManager.loadedUnlocked ? 1 : 0);
        PlayerPrefs.SetInt("invulnerableUnlocked", achievementManager.invulnerableUnlocked ? 1 : 0);
        PlayerPrefs.SetInt("cuttingItCloseUnlocked", achievementManager.cuttingItCloseUnlocked ? 1 : 0);
        PlayerPrefs.SetFloat("damageTaken", achievementManager.damageTaken);
        PlayerPrefs.SetFloat("explodedEnemies", achievementManager.explodedEnemies);
        PlayerPrefs.SetFloat("explodesUnlocked", achievementManager.explodesUnlocked);
    }

    private void LoadAchievementManagerStats() {
        achievementManager.unlockedWeapons = PlayerPrefs.GetInt("unlockedWeapons");
        achievementManager.upgradedAbilities = PlayerPrefs.GetInt("upgradedAbilities");
        achievementManager.numKills = PlayerPrefs.GetInt("numKills");
        achievementManager.killsUnlocked = PlayerPrefs.GetInt("killsUnlocked");
        achievementManager.limbsUnlocked = PlayerPrefs.GetInt("limbsUnlocked");
        achievementManager.damageTakenUnlocked = PlayerPrefs.GetInt("damageTakenUnlocked");
        achievementManager.repairUnlocked = PlayerPrefs.GetInt("repairUnlocked");
        achievementManager.earnedCash = PlayerPrefs.GetInt("earnedCash");
        achievementManager.wavesUnlocked = PlayerPrefs.GetInt("wavesUnlocked");
        achievementManager.loadedUnlocked = PlayerPrefs.GetInt("loadedUnlocked") == 1;
        achievementManager.invulnerableUnlocked = PlayerPrefs.GetInt("invulnerableUnlocked") == 1;
        achievementManager.cuttingItCloseUnlocked = PlayerPrefs.GetInt("cuttingItCloseUnlocked") == 1;
        achievementManager.damageTaken = PlayerPrefs.GetFloat("damageTaken");
        achievementManager.explodedEnemies = PlayerPrefs.GetFloat("explodedEnemies");
        achievementManager.explodesUnlocked = PlayerPrefs.GetFloat("explodesUnlocked");
    }

    private void SaveMotorbikeStats() {
        PlayerPrefs.SetFloat("healthRepaired", motorbike.healthRepaired);
    }

    private void LoadMotorbikeStats() {
        motorbike.healthRepaired = PlayerPrefs.GetFloat("healthRepaired");
    }

    private void SaveAbilityButtonStats() {
        foreach (AbilityButton abilityButton in abilityButtons) {
            PlayerPrefs.SetInt(abilityButton.name, abilityButton.currLevel);
        }
    }

    private void LoadAbilityButtonStats() {

        foreach (AbilityButton abilityButton in abilityButtons) {
            abilityButton.currLevel = PlayerPrefs.GetInt(abilityButton.name);
            for (int i = 0; i < abilityButton.currLevel; i++) {
                GameObject.Find(abilityButton.gameObject.name + "UpgradeIcons/UpgradeLvl" + i).GetComponent<Image>().sprite = purchasedSprite;
            }
        }
    }

    private void SaveWeaponChangerStats() {
        PlayerPrefs.SetString("activeWeapon", weaponChanger.GetCurrentWeapon().name);
    }

    private void LoadWeaponChangerStats() {
        weaponChanger.EnableWeapon(PlayerPrefs.GetString("activeWeapon"));
    }

    private void SaveWeaponStats() {
        foreach (WeaponController weapon in weapons) {
            PlayerPrefs.SetFloat(weapon.name + "damage", weapon.damage);
            PlayerPrefs.SetFloat(weapon.name + "numProjectiles", weapon.numProjectiles);
            PlayerPrefs.SetFloat(weapon.name + "reloadTime", weapon.reloadTime);
            PlayerPrefs.SetFloat(weapon.name + "delayBase", weapon.delayBase);
            PlayerPrefs.SetFloat(weapon.name + "accuracyModMax", weapon.accuracyModMax);
            PlayerPrefs.SetFloat(weapon.name + "accuracyModMin", weapon.accuracyModMin);
            PlayerPrefs.SetFloat(weapon.name + "delay", weapon.delay);
            PlayerPrefs.SetFloat(weapon.name + "accuracyMod", weapon.accuracyMod);
            PlayerPrefs.SetInt(weapon.name + "unlockedUpgrades", weapon.unlockedUpgrades);
            PlayerPrefs.SetInt(weapon.name + "isUnlocked", weapon.isUnlocked ? 1 : 0);
            PlayerPrefs.SetInt(weapon.name + "capacity", weapon.capacity);
            PlayerPrefs.SetInt(weapon.name + "roundsLoaded", weapon.roundsLoaded);
        }
    }

    private void LoadWeaponStats() {
        foreach (WeaponController weapon in weapons) {
            weapon.damage = PlayerPrefs.GetFloat(weapon.name + "damage");
            weapon.numProjectiles = PlayerPrefs.GetFloat(weapon.name + "numProjectiles");
            weapon.reloadTime = PlayerPrefs.GetFloat(weapon.name + "reloadTime");
            weapon.delayBase = PlayerPrefs.GetFloat(weapon.name + "delayBase");
            weapon.accuracyModMax = PlayerPrefs.GetFloat(weapon.name + "accuracyModMax");
            weapon.accuracyModMin = PlayerPrefs.GetFloat(weapon.name + "accuracyModMin");
            weapon.delay = PlayerPrefs.GetFloat(weapon.name + "delay");
            weapon.accuracyMod = PlayerPrefs.GetFloat(weapon.name + "accuracyMod");
            weapon.unlockedUpgrades = PlayerPrefs.GetInt(weapon.name + "unlockedUpgrades");
            weapon.isUnlocked = PlayerPrefs.GetInt(weapon.name + "isUnlocked") == 1;
            weapon.capacity = PlayerPrefs.GetInt(weapon.name + "capacity");
            weapon.roundsLoaded = PlayerPrefs.GetInt(weapon.name + "roundsLoaded");
        }
    }

    private void SaveWeaponUpgrades() {
        foreach (UpgradeButton weaponUpgrade in weaponUpgrades) {
            PlayerPrefs.SetInt(weaponUpgrade.transform.parent.name + weaponUpgrade.name + "currLevel", weaponUpgrade.currLevel);
        }
    }

    private void LoadWeaponUpgrades() {
        foreach (UpgradeButton weaponUpgrade in weaponUpgrades) {
            weaponUpgrade.currLevel = PlayerPrefs.GetInt(weaponUpgrade.transform.parent.name + weaponUpgrade.name + "currLevel");
            for (int i = 0; i < weaponUpgrade.currLevel; i++) {

                GameObject.Find(weaponUpgrade.transform.parent.name + weaponUpgrade.name + "/UpgradeLvl" + i).GetComponent<Image>().sprite = purchasedSprite;
            }
        }
    }

    private void SaveSpawnStats() {
        PlayerPrefs.SetInt("currentWave", spawnerManager.currentWave);
    }

    private void LoadSpawnStats() {
        spawnerManager.currentWave = PlayerPrefs.GetInt("currentWave");
        spawnerManager.UpdateWaveButton();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Save();
        }

        if (Input.GetKeyDown(KeyCode.Backspace)) {
            Load();
        }
    }

    public void Clear() {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
    }

}
