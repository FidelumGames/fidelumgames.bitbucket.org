﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Credits : MonoBehaviour {

    private Image panelBG;
    private Color color;
    private GameObject creditsText;
    private float maxAlpha = .5f;
    private bool creditsStarted = false;
    public float fadeInSpeed = .25f;
    public float scrollSpeed = 1f;
    public float maxYPos = 1160;
    private Vector3 posTemp;

    private string credits = "Pixel zombie shooter "
                            + "\n"
                            + "\nDesign, Programming, Primary Artist"
                            + "\nJake Jollimore"
                            + "\n"
                            + "\nAdditional Artwork From OpenGameArt.Org By"
                            + "\nansimuz"
                            + "\n"
                            + "\nMusic"
                            + "\nLinouille 8-Bit"
                            + "\n"
                            + "\nSFX From FreeSound.org by"
                            + "\nCydon"
                            + "\ninspectorj"
                            + "\nvixuxx"
                            + "\npauliep"
                            + "\nnettimato"
                            + "\nerdie"
                            + "\ndavidworksonline"
                            + "\njivatma07"
                            + "\nmichel88"
                            + "\nbmaczero"
                            + "\nautistic  lucario"
                            + "\narrigd"
                            + "\ntcawte"
                            + "\nunder7dude"
                            + "\nmojomills"
                            + "\ncapslok"
                            + "\nsuntemple.co"
                            + "\nlittlerobotsoundfactory"
                            + "\nhonorhunter (Colin Andrew Grant)"
                            + "\nSound Set Recordings"
                            + "\nastrand"
                            + "\nartmasterrich"
                            + "\n"
                            + "\nPlaytesting"
                            + "\nAwoken"
                            + "\nLinouille 8-bit"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "\nFor C, K and L.";

	void Start () {
        panelBG = GetComponent<Image>();
        color = panelBG.color;
        color.a = 0;
        panelBG.color = color;
        creditsText = GetComponentInChildren<Text>().gameObject;
        creditsText.GetComponent<Text>().text = credits;
        posTemp = creditsText.transform.localPosition;
	}
	
	void Update () {
        if (creditsStarted) {
            if(color.a < maxAlpha && posTemp.y < maxYPos) {
                color.a += fadeInSpeed * Time.deltaTime;
                panelBG.color = color;
            } else if(posTemp.y < maxYPos){
                posTemp.y += scrollSpeed * Time.deltaTime;
                creditsText.transform.localPosition = posTemp;
            } else {
                color.a -= fadeInSpeed * Time.deltaTime;
                panelBG.color = color;
                if(color.a <= 0) {
                    GameObject.Find("IntroObjects").GetComponent<Animator>().SetBool("End", true);
                    Destroy(gameObject);
                }
            }
        }
	}

    public void StartCredits() {
        creditsStarted = true;
    }
}
